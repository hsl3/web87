import { createApp } from 'vue'
import App from './App.vue'

// element-plus 支持vue3的ui组件库，使用和element-ui一致
import ElementUI from 'element-plus'
import 'element-plus/dist/index.css'
// use(ElementUI) 使用组件库
createApp(App).use(ElementUI).mount('#app')


