/* 类型注解 */
var n1: number = 123
var n2: string = "123"
var n3: (number | string)[] = [1234, '2222']
var n4: Array<number | string> = [1234, '1']
/* 联合类型 */
var n5: number | string = '1'

/* 类型别名 */
type T = {
  id: number
}
var obj: T = { id: 1 }

/* 函数类型 */
function add(a: number, b: number): number {
  return a
}
add(1, 2)

const add2 = (a: number, b: number): number => {
  return a
}
add2(1, 2)

const add3: (a: number, b: number) => number = (a, b) => {
  return a
}
add3(1, 2)

/* 对象类型 */
const obj2: {
  id: number
  arr: number[]
  fn(): void
  fn2: () => void
} = {
  id: 1,
  arr: [],
  fn() { },
  fn2: () => { }
}

/* typeof keyof */
const obj3 = {
  id: 1,
  arr: [],
  fn() { },
  fn2: () => { }
}


type T2 = typeof obj3  //  值类型
type T3 = keyof T2 //  键类型

/* 类型推断 */
var n6 = 123
/* 类型断言 as */
document.getElementById('#box') as HTMLAnchorElement
var obj4 = {} as T2
obj4.id = 1