import { defineStore } from 'pinia'

export const useStoreNumber = defineStore('number', {
  state: () => {
    return {
      n: 123,
      arr: [1, 2, 3, 4, 5]
    }
  },
  actions: {
    add() {
      // this===store
      console.log(this.n, 222);
      this.n += 5
    }

  },
  getters: {
    // total(): number {
    //   return this.arr.reduce((sum, item) => sum + item, 0)
    // }
    total(state) {
      return state.arr.reduce((sum, item) => sum + item, 0)
    }
  }
})