import { defineStore } from 'pinia'
import { ref, computed } from 'vue'
// defineStore(/* 模块的名字唯一值类似id,函数 */)====返回值是一个函数:===建议取名字：useStore+模块名
export const useStoreCounter = defineStore('counter', (): any => {
  /* state */
  const count = ref(10)
  /* mutations */
  const add = () => {
    count.value += 2
  }
  /* actions */
  const countAction = () => {
    setTimeout(() => {
      count.value += 3
      console.log(count.value, 7777);
    }, 3000)
  }

  /* getters */
  const countGetters = computed(() => {
    return count.value + 10
  })
  return { count, add, countGetters, countAction }
})