import { defineStore } from 'pinia'
import { ref } from 'vue'
export const useStoreTt = defineStore('tt', () => {
  const activeId = ref(2)

  /* 定义切换高亮函数 */
  const changeId = (e: number) => {
    activeId.value = e
  }
  return { activeId, changeId }
})