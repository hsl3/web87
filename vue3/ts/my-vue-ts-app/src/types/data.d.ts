export type T = {
  id: number
  name: string
}

export type ChannelItem = {
  id: number;
  name: string;
};
// 频道接口响应数据
export type ChannelResData = {
  data: {
    channels: ChannelItem[]
  },
  message: string
}


export type ArticleItem = {
  art_id: number
  title: string
  comm_count: number
  pubdate: string
  aut_name: string
  cover?: {
    type: number
  }
};
// 频道接口响应数据
export type ArticleResData = {
  data: {
    results: ArticleItem[]
  },
  message: string
}
