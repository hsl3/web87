import { createApp } from 'vue'
import './style.css'
import App from './App.vue'

import router from './router'
import store from './store'
// import { add, point, userInfo } from './add';
// add(1, 2)
// console.log(userInfo.name);

/* 导入pinia实例对象方法 */
import { createPinia } from 'pinia'
/* 获取实例对象 */
const pinia = createPinia()

createApp(App).use(pinia).use(router).use(store).mount('#app')
// const app = createApp(App)
// app.use(pinia).mount('#app')
