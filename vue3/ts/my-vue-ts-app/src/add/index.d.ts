declare const add: (a: number, b: number) => number

type Point = {
  x: number
  y: number
}
declare const point: (p: Point) => void

type user = {
  id: number,
  name: string
}
declare const userInfo: user
export { add, point, userInfo };