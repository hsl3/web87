const add = (a, b) => {
  return a + b;
};

const point = (p) => {
  console.log('坐标：', p.x, p.y);
};

const userInfo = {
  id: 1,
  name: 'jack'
}

export { add, point, userInfo }
