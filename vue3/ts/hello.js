"use strict";
exports.__esModule = true;
/* 数组类型 */
var arr = [1, 2, 3, 4];
var arr2 = [1, 2, 3, 4];
var arr3 = ['1', '2'];
/* 联合类型 */
var n = 1;
n = '2';
var m = { id: 1, name: 'jack' };
m = [1];
m = 2;
m = { id: 2, name: 'jack' };
var arr4 = [1, 2, 3, '4', ['2']];
/*
var  list=[
  {
    id:1,
    name:'jack',
    hobby:['游泳'],
    family:{
      mather:'妈妈'
    }
  }
]
*/
var list = [
    {
        id: 1,
        name: 'jack',
        hobby: ['游泳'],
        family: {
            mather: '妈妈'
        }
    }
];
var m3 = { id: 1, name: 'jack' };
var list = [
    {
        id: 1,
        name: 'jack',
        hobby: ['游泳'],
        family: {
            mather: '妈妈'
        }
    }
];
var list2 = [
    {
        id: 1,
        name: 'jack',
        hobby: ['游泳'],
        family: {
            mather: '妈妈'
        },
        say: function () {
            return 123;
        },
        fn: function () {
            return 222;
        }
    }
];
// void类型 对于返回值不确定的可以使用 void
// type F = (a: number, b: number) => undefined
// const add: F = (a, b) => {
//   return undefined
// }
// // console.log(add(1, 2));
// add(1, 2)
/* 对象类型 */
// const obj2: {
//   id: 2,
//   age: number,
//   name: string
// } = {
//   id: 34,
//   age: 18,
//   name: 'javk'
// }
/* 字面量类型 */
// var num2: 123 = 123
// const num5 = 1234
// var num6: 1234 = 1234
/* 可选参数 */
var obj2 = {
    id: 34,
    age: 19,
    // say: () => {
    //   return 123
    // },
    fn: function () {
        return 222;
    }
};
var obj4 = {
    id: 1,
    name: 'tom',
    age: 19,
    hobby: ['rap']
};
/* type继承  交叉类型  & */
// type Tperson = {
//   id: number
//   name: string
// }
// type Tperson2 = Tperson & {
//   age: number
//   hobby: string[]
// }
// const obj5: Tperson2 = {
//   id: 1,
//   name: '小黑子',
//   age: 19,
//   hobby: ['rap']
// }
/* 类型推断 */
var num5 = 123;
var num6 = '`1223';
var add = function (num1, num2) {
    return num1 + num2;
};
add(1, '2');
// add = 2
/* 类型断言   as  */
var aLink = document.getElementById('link');
var aLink2 = document.getElementById('link'); // 表示获取的是一个id为link的  并且是一个a标签
var aLink3 = document.getElementById('link'); // 表示获取的是一个id为link的  并且是一个a标签
/*
类型注解   var n:number=1233
类型推断   var  num=123
类型断言as    const aLink2 = document.getElementById('link') as HTMLAnchorElement
类型别名   type Person={id:number}
接口 interface   interface Person {id:number}
联合类型   var n:number|string|numer[]=1234
数组类型  Array<number|string>  (numer|string)[]  var arr=[1,2,'3']
*/
/* typeof */
// type Person3 = {
//   id: number
//   age: number
// }
// const obj3 = {
//   id: 1,
//   age: 18
// }
// /* typeof 值===》返回的是类型 */
// type Person4 = typeof obj3
// const obj6 = {} as Person4
// obj6.id = 1
/*
data:{
  obj4:{}  //{id:1,name:'jack',age:18}
}

*/
var res = {
    info: 'admin',
    introduce: '这是一个。。。。',
    mobile: 1233
};
var objiNFO = {};
var p1 = 'introduce';
/*
 id: number,
  age: number,
  hobby: string[],
  res:{
    id: number;
    goodsName: string;
  }
*/
var obj7 = {
    id: 1,
    age: 12,
    hobby: ['2222'],
    res: {
        id: 1,
        goodsName: 'jack'
    }
};
// const obj8: Iperson6 = {
//   code: 1,
//   messgae: '2',
//   data: 1
// }
var obj8 = {
    code: 1,
    messgae: 3,
    data: 1
};
// function add4(a: F): F {
//   return a
// }
// add4(1)
// function add4<F, SS>(a: F, b?: SS): F {
//   return a
// }
// // add4('123')
// add4(1111, 22222)
// function add4<F extends F1>(a: F): F {
//   return a
// }
// add4(1)
// const c1 = { id: 1 }
// typeof c1==={id:number}
/* F=>{id:number} */
// function add4<F extends typeof c1>(a: F): F {
//   return a
// }
// add4({ id: 3 })
// const c1 = { id: 1 }
// // keyof typeof c1  ===keyof {id:number}====id
// function add4<F extends keyof typeof c1>(a: F): F {
//   return a
// }
// add4('id')
// type Direction = "up" | "down" | "left" | "right"
// function add14(d: Direction) {
//   return d
// }
// add14('right')
/* 枚举 */
var Day;
(function (Day) {
    Day[Day["SUNDAY"] = 0] = "SUNDAY";
    Day[Day["MONDAY"] = 10] = "MONDAY";
    Day[Day["TUESDAY"] = 11] = "TUESDAY";
    Day["WEDNESDAY"] = "WEDNESDAY";
    Day["THURSDAY"] = "THURSDAY";
    Day["FRIDAY"] = "FRIDAY";
    Day["SATURDAY"] = "SATURDAY";
})(Day || (Day = {}));
function add15(Day) {
    return Day;
}
add15(Day.MONDAY);
