
export { }
/* 数组类型 */

let arr: Array<number> = [1, 2, 3, 4]
let arr2: number[] = [1, 2, 3, 4]
let arr3: string[] = ['1', '2']

/* 联合类型 */

var n: number | string = 1
n = '2'

var m: { id: number, name: string } | number | number[] = { id: 1, name: 'jack' }
m = [1]
m = 2
m = { id: 2, name: 'jack' }

let arr4: (number | string | string[])[] = [1, 2, 3, '4', ['2']]

/*
var  list=[
  {
    id:1,
    name:'jack',
    hobby:['游泳'],
    family:{
      mather:'妈妈'
    }
  }
]
*/

var list: {
  id: number;
  name: string;
  hobby: string[];
  family: {
    mather: string;
  };
}[] = [
    {
      id: 1,
      name: 'jack',
      hobby: ['游泳'],
      family: {
        mather: '妈妈'
      }
    }
  ]

/* 类型别名 */
type T = { id: number, name: string } | number | number[]
var m3: T = { id: 1, name: 'jack' }


type N = number
type S = string

type L = {
  id: N;
  name: S;
  hobby: S[];
  family: {
    mather: S;
  };
}[]

var list: L = [
  {
    id: 1,
    name: 'jack',
    hobby: ['游泳'],
    family: {
      mather: '妈妈'
    }
  }
]

/* 函数类型 */
// function add(a: number, b: number): string {
//   return '1'
// }

// add(1, 2)

// const add = (a: number, b: number): string => {
//   return '2'
// }
// add(1, 2)

// type F = (a: number, b: number) => string
// // const add: F = (a, b) => {
// //   return '12'
// // }
// // add(1, 2)



type List = [
  {
    id: number
    name: string
    hobby: string[]
    family: {
      mather: string
    },
    say: () => number
    fn(): number
  }
]
var list2: List = [
  {
    id: 1,
    name: 'jack',
    hobby: ['游泳'],
    family: {
      mather: '妈妈'
    },
    say: () => {
      return 123
    },
    fn() {
      return 222
    }
  }
]


// void类型 对于返回值不确定的可以使用 void
// type F = (a: number, b: number) => undefined
// const add: F = (a, b) => {
//   return undefined
// }
// // console.log(add(1, 2));
// add(1, 2)


/* 对象类型 */
// const obj2: {
//   id: 2,
//   age: number,
//   name: string

// } = {
//   id: 34,
//   age: 18,
//   name: 'javk'
// }

/* 字面量类型 */
// var num2: 123 = 123
// const num5 = 1234
// var num6: 1234 = 1234

/* 可选参数 */
const obj2: {
  id: 34
  age?: number,
  say?: () => number
  fn?(): void
  name?: string // 可选
} = {
  id: 34,
  age: 19,
  // say: () => {
  //   return 123
  // },
  fn() {
    return 222
  }
}

// type F = (a?: number, b?: number) => undefined
// const add: F = (a, b) => {
//   return undefined
// }
// // console.log(add(1, 2));
// add()


/* 接口  interface  定义对象数据类型 */
// type P = {
//   id: number
//   name: string
// }
// const obj3: P = {
//   id: 1,
//   name: 'rose'
// }


// interface 继承
interface Iperson {
  id: number
  name: string
}
// const obj3: Iperson = {
//   id: 1,
//   name: 'rose'
// }

// /* 接口  interface 继承  extends*/
// interface Iperson2 extends Iperson {
//   age: number
//   hobby: string[]
// }

interface Iperson {
  age: number
  hobby: string[]
}

const obj4: Iperson = {
  id: 1,
  name: 'tom',
  age: 19,
  hobby: ['rap']
}


/* type继承  交叉类型  & */
// type Tperson = {
//   id: number
//   name: string
// }

// type Tperson2 = Tperson & {
//   age: number
//   hobby: string[]
// }

// const obj5: Tperson2 = {
//   id: 1,
//   name: '小黑子',
//   age: 19,
//   hobby: ['rap']
// }


/* 类型推断 */
var num5 = 123
var num6 = '`1223'

var add: any = (num1, num2) => {
  return num1 + num2;
};

add(1, '2')
// add = 2

/* 类型断言   as  */
const aLink = document.getElementById('link')
const aLink2 = document.getElementById('link') as HTMLAnchorElement // 表示获取的是一个id为link的  并且是一个a标签
const aLink3 = document.getElementById('link') as HTMLDivElement // 表示获取的是一个id为link的  并且是一个a标签

/*
类型注解   var n:number=1233
类型推断   var  num=123
类型断言as    const aLink2 = document.getElementById('link') as HTMLAnchorElement
类型别名   type Person={id:number}
接口 interface   interface Person {id:number}
联合类型   var n:number|string|numer[]=1234
数组类型  Array<number|string>  (numer|string)[]  var arr=[1,2,'3']
*/


/* typeof */

// type Person3 = {
//   id: number
//   age: number
// }

// const obj3 = {
//   id: 1,
//   age: 18
// }


// /* typeof 值===》返回的是类型 */
// type Person4 = typeof obj3

// const obj6 = {} as Person4
// obj6.id = 1

/*
data:{
  obj4:{}  //{id:1,name:'jack',age:18}
}

*/

const res = {
  info: 'admin',
  introduce: '这是一个。。。。',
  mobile: 1233
}

type TT = typeof res
const objiNFO = {} as TT


/* keyof  */
// keyof 类型  ===返回的是类型
type PP = keyof TT
var p1: PP = 'introduce'

/* typeof 值===》返回的是类型 */
// keyof 类型  ===返回的是类型 中的键组成的字面量类型

/* 泛型===类型别名 */
type User = {
  name: string;
  age: number;
}

type Goods = {
  id: number;
  goodsName: string;
}

type Data<T, S = number> = {
  id: number,
  age: number,
  hobby: string[],
  res: T,
  code?: S
}

type userData = Data<number>
type userData2 = Data<string>
type userData3 = Data<Goods>
/*
 id: number,
  age: number,
  hobby: string[],
  res:{
    id: number;
    goodsName: string;
  }
*/
const obj7: userData3 = {
  id: 1,
  age: 12,
  hobby: ['2222'],
  res: {
    id: 1,
    goodsName: 'jack'
  }
}

/* 小程序 */
/* 
{
  code:10000,
  messgae:'成功',
  data:[]||{}
}

*/

type S1<T> = {
  code: number,
  messgae: string,
  data: T
}
type S2 = S1<string[]>


/* 泛型==接口 */
interface Iperson5<T, S = string> {
  code: number,
  messgae: S,
  data: T
}

type Iperson6 = Iperson5<number>
// const obj8: Iperson6 = {
//   code: 1,
//   messgae: '2',
//   data: 1
// }

const obj8: Iperson5<number, number> = {
  code: 1,
  messgae: 3,
  data: 1
}

/* 泛型   函数 */
// function add4(a: T, b: S): S {
//   return b
// }

// add4(1, '2')
type F1 = number
// function add4(a: F): F {
//   return a
// }
// add4(1)

// function add4<F, SS>(a: F, b?: SS): F {
//   return a
// }
// // add4('123')
// add4(1111, 22222)

// function add4<F extends F1>(a: F): F {
//   return a
// }
// add4(1)

// const c1 = { id: 1 }
// typeof c1==={id:number}
/* F=>{id:number} */
// function add4<F extends typeof c1>(a: F): F {
//   return a
// }
// add4({ id: 3 })

// const c1 = { id: 1 }
// // keyof typeof c1  ===keyof {id:number}====id
// function add4<F extends keyof typeof c1>(a: F): F {
//   return a
// }

// add4('id')

// type Direction = "up" | "down" | "left" | "right"
// function add14(d: Direction) {
//   return d
// }

// add14('right')


/* 枚举 */
enum Day {
  SUNDAY,
  MONDAY = 10,
  TUESDAY,
  WEDNESDAY = 'WEDNESDAY',
  THURSDAY = 'THURSDAY',
  FRIDAY = 'FRIDAY',
  SATURDAY = 'SATURDAY'
}
function add15(Day: Day) {
  return Day
}
add15(Day.MONDAY)