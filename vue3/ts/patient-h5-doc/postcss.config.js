// eslint-disable-next-line no-undef
module.exports = {
  plugins: {
    'postcss-px-to-viewport': {
      // 设备宽度375计算vw的值
      /* 设计稿是多少就写多少px  会自动转换成vw */
      viewportWidth: 375
    }
  }
}
