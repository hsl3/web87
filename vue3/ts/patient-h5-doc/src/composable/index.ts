import type { Doctor, FollowType } from '@/types/consult'
import { followOrUnfollow } from '@/services/consult'

export const useFollow = (type: FollowType = 'doc') => {
  const follow = async (item: { id: string; likeFlag: 0 | 1 }) => {
    await followOrUnfollow(item.id, type)
    item.likeFlag = item.likeFlag == 1 ? 0 : 1
  }
  return { follow }
}

//  useFollow()
