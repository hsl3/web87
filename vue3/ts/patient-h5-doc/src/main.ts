import { createApp } from 'vue'

import App from './App.vue'
import router from './router'
// 2. 引入组件样式
import 'vant/lib/index.css'
// import './assets/main.css'
import './styles/main.scss'
import pinia from './stores'

import 'virtual:svg-icons-register'
import direc from '@/directives'
console.log(direc, 888)

export const app = createApp(App)

// app.directive('errImg', {
//   /* 自定义指令钩子 */

//   mounted(el, { value }) {
//     console.log(el, 1999)
//     // console.log(binding, 1999)
//     el.onerror = () => {
//       console.log(22222)
//       el.src = value
//     }
//   }
// })

app.use(direc)
app.use(router)
app.use(pinia)

app.mount('#app')
