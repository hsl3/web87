import { request } from '@/utils/request'
import type {
  User,
  CodeType,
  UserInfo,
  PatientList,
  Patient
} from '@/types/user'

export const loginByPassword = (password: string, mobile: string) => {
  return request<User>('login/password', 'POST', {
    password,
    mobile
  })
}

export const sendMobileCode = (mobile: string, type: CodeType) =>
  request('/code', 'GET', { mobile, type })

export const loginByCode = (mobile: string, code: string) =>
  request<User>('/login', 'POST', { mobile, code })

// 获取个人信息
export const getUserInfo = () => {
  return request<UserInfo>('/patient/myUser')
}
/* 问诊-查询患者列表信息 */
export const getPatientList = () => {
  return request<PatientList>('/patient/mylist')
}
// 添加患者信息
export const addPatient = (patient: Patient) =>
  request('/patient/add', 'POST', patient)
// 编辑患者信息
export const editPatient = (patient: Patient) =>
  request('/patient/update', 'PUT', patient)
// 删除患者信息
export const delPatient = (id: string) =>
  request(`/patient/del/${id}`, 'DELETE')

// 查询患者详情
export const getPatientDetail = (id: string) =>
  request<Patient>(`/patient/info/${id}`)
