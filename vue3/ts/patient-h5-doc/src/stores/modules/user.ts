import { defineStore } from 'pinia'
import type { User } from '@/types/user'
import { ref } from 'vue'

export const useStoreUser = defineStore(
  'user',
  () => {
    const user = ref<User>()

    const setUser = (u: User) => {
      user.value = u
    }

    const delUser = () => {
      user.value = undefined
    }
    return { user, setUser, delUser }
  },
  {
    persist: true
  }
)
// export default useStoreUser
