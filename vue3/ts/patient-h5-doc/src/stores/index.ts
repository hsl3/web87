/* 持久化 */
import persist from 'pinia-plugin-persistedstate'
import { createPinia } from 'pinia'
export * from './modules/user'
export * from './modules/consult'
export * from './modules/counter'  // useCounterStore
const pinia = createPinia()
pinia.use(persist)
export default pinia
