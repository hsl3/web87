import { onMounted, onUnmounted, ref } from 'vue'

export const useWindowSize = () => {
  const width = ref(0)
  const height = ref(0)
  const setWidth = () => {
    width.value = window.innerWidth
    height.value = window.innerHeight
  }
  onMounted(() => {
    setWidth()
    window.addEventListener('resize', setWidth)
  })
  onUnmounted(() => {
    window.removeEventListener('resize', setWidth)
  })
  return { width, height }
}
