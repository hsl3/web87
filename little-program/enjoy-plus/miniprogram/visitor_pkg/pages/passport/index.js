Page({
  onShareAppMessage (e) {
    console.log(e.from, 111);
    return {
      title: '查看通行证',
      path: '/visitor_pkg/pages/passport/index',
      imageUrl: 'https://enjoy-plus.oss-cn-beijing.aliyuncs.com/images/share_poster.png',
    }
  },
  share () {
    console.log(22222);

  },
  getPhoneNumber (e) {
    wx.utils.toast(e.detail.code)
    console.log(e.detail.code, 888)
  },
  onLoad ({ id }) {
    //  控制分享显示去哪里
    // wx.showShareMenu({
    //   withShareTicket: true,
    //   menus: ['shareAppMessage', 'shareTimeline'],
    //   success(e) {
    //     console.log(e, 333);
    //   }
    // })
    console.log(id, 777);
    this.getPassport(id)
  },
  async getPassport (id) {
    if (!id) return
    // 请求数据接口
    const { code, data: passport } = await wx.http.get('/visitor/' + id)
    // 检测接口调用的结果
    if (code !== 10000) return wx.utils.toast('获取通行证失败!')
    // 渲染通行证
    this.setData({ ...passport })
  }
})
