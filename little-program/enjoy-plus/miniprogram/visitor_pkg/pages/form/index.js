Page({
  data: {
    name: '',
    gender: "1",
    mobile: '10086',
    visitDate: "",
    currentDate: '请选择时间',
    minDate: new Date().getTime(),

    dateLayerVisible: false,
    houseLayerVisible: false,
    houseList: [
      { name: '北京西三旗花园1号楼 101' },
      { name: '北京东村家园3号楼 302' },
      { name: '北京育新花园3号楼 703' },
      { name: '北京天通苑北苑8号楼 403' },
    ],
  },
  openHouseLayer () {
    this.setData({ houseLayerVisible: true })
  },
  closeHouseLayer () {
    this.setData({ houseLayerVisible: false })
  },
  openDateLayer () {
    this.setData({ dateLayerVisible: true })
  },
  closeDateLayer () {
    this.setData({ dateLayerVisible: false })
  },
  goPassport () {
    wx.reLaunch({
      url: '/visitor_pkg/pages/passport/index',
    })
  },
  onLoad () {
    this.getHouseList()
  },
  /* 选择房屋 */
  selectHouse (ev) {
    console.log(ev, 8888);
    // 获取房到的信息数据
    const { id: houseId, name: houseInfo } = ev.detail
    // // 页面中渲染
    this.setData({ houseId, houseInfo })
  },
  async getHouseList () {
    // 请求数据接口
    const { code, data: houseList } = await wx.http.get('/house')
    // 检测接口返回的结果
    if (code !== 10000) return wx.utils.toast('获取房屋列表失败!')
    // 数据渲染
    this.setData({ houseList })
  },
  selectData ({ detail }) {
    console.log(detail, 7777);
    this.setData({
      visitDate: wx.formatDate(detail),
      dateLayerVisible: false
    });
  },
  async submitForm () {
    /* 发布访客信息 */
    const { houseId, name, gender, mobile, visitDate } = this.data
    const { code, data } = await wx.http.post('/visitor', { houseId, name, gender, mobile, visitDate })
    console.log(code, data, 888);
    if (code !== 10000) return wx.utils.toast('添加访问失败')
    wx.navigateTo({ url: '/visitor_pkg/pages/list/index' })
  }
})