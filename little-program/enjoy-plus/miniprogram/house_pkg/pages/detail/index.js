Page({
  editHouse () {
    wx.navigateTo({
      url: '/house_pkg/pages/form/index?id=' + this.data.id,
    })
  },
  onLoad ({ id }) {
    console.log(id, 88);
    this.setData({ id })
    this.getHouseDetail(id)
  },
  async getHouseDetail (id) {
    if (!id) return
    // 请求数据接口
    const { code, data: houseDetail } = await wx.http.get('/room/' + id)
    // 检测接口返回的结果
    if (code !== 10000) return wx.utils.toast()
    // 渲染数据
    this.setData({ ...houseDetail })//  data:{gender:1}
  },
})
