// house_pkg/pages/building/index.ts
Page({

  /**
   * 页面的初始数据
   */
  data: {
    point: '',//  小区名字
    size: 5,//模拟小区有5栋楼   随机取值
    type: ''//  大于4：几号楼  否则：几栋
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad ({ point }) {
    console.log(point, 88);
    var size = Math.floor(Math.random() * 5 + 1)  //[0,5] 
    var type = size > 4 ? '号楼' : '栋'  //'号楼' || '栋'
    this.setData({
      point,
      size,
      type
    })
    //  point+ size+type  青龙社区5号楼   新桥小区3栋
  },
  goRoom ({ mark: { point, building } }) {
    // console.log(e, 5566);
    wx.navigateTo({
      url: `/house_pkg/pages/room/index?point=${point}&building=${building}`,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage () {

  }
})