// house_pkg/pages/room/index.ts
Page({

  /**
   * 页面的初始数据
   */
  data: {
    rooms: [],
    point: '',
    building: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad ({ point, building }) {
    console.log(point, building, 88);
    this.fake()
    this.setData({
      point,
      building
    })

  },
  /* 生成房间号 */
  fake () {
    /*
    2003:20层03房间
    1-生成楼层     7   11   18   28   33
    2-生成房间号  需要补0   一层最多4
    */
    var size = Math.floor(Math.random() * 5 + 4) //  <9  表示获取的数据小于9
    var rooms = []
    for (let i = 0; i < size; i++) {
      var floor = Math.floor(Math.random() * 18 + 1)  //18
      var No = Math.floor(Math.random() * 4 + 1) //  4
      var room = [floor, 0, No].join('')//1801
      // 下畈新苑 3号楼1801  这里只是一层：每一层每一户
      /* 去重 */
      if (rooms.includes(room)) continue
      rooms.push(room) // 收集房间号
    }

    // var floor = Math.floor(Math.random() * 18 + 1)  //18
    // var rooms = []
    // for (var i = 0; i < floor; i++) {
    //   for (var j = 0; j < 4; j++) {
    //     var room = i + 1 + '0' + j + 1 + '房间'
    //     rooms.push(room)
    //   }
    // }

    console.log(rooms, 65511);
    this.setData({
      rooms
    })

  },

  roomDetail ({ mark: { room } }) {
    wx.navigateTo(
      {
        url: `/house_pkg/pages/form/index?point=${this.data.point}&building=${this.data.building}&room=${room}`
      }
    )
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage () {

  }
})