Page({
  // data: {
  //   idcardFrontUrl: '/static/images/avatar_1.jpg',
  //   idcardBackUrl: '/static/images/avatar_2.jpg',
  // },
  // goList () {
  //   wx.reLaunch({
  //     url: '/house_pkg/pages/list/index',
  //   })
  // },
  removePicture (ev) {
    // 移除图片的类型（身份证正面或反面）
    const type = ev.mark.type //idcardFrontUrl|| idcardBackUrl
    this.setData({ [type]: '' })
  },

  data: {

    gender: '1',
    name: '杰克',
    mobile: '13333333333',
    idcardFrontUrl: '',
    idcardBackUrl: ''
  },
  onChange (event) {
    this.setData({
      gender: event.detail,
    });
  },

  chooseImg (e) {
    console.log(e, 777);
    var type = e.mark.idcardUrl  //idcardFrontUrl|| idcardBackUrl
    wx.chooseMedia({
      count: 1,
      mediaType: ['image'],
      sourceType: ['album', 'camera'],
      maxDuration: 30,
      camera: 'back',
      success: (res) => {
        // 调用接口上传图片
        wx.uploadFile({
          url: wx.http.baseURL + '/upload',
          filePath: res.tempFiles[0].tempFilePath,
          name: 'file',
          header: {
            Authorization: 'Bearer ' + getApp().token,
          },
          success: (res) => {
            // 返回值是401  根据是401  就去刷新token获取新的token
            console.log(res, 8877);
            // 转换 json 数据
            const data = JSON.parse(res.data)
            // // 检测接口调用结果
            if (data.code !== 10000) return wx.utils.toast('上传图片失败!')
            // // 保存并预览图片地址
            this.setData({
              [type]: data.data.url,
            })
          },
        })
      }
    })
  },


  /* 提交 */
  async submitForm () {
    /*
    1-校验姓名
    2-校验手机号
    3-校验图片是否选择了
    */
    /* 必须是中文输入字符是2-5 */
    // 1-校验姓名
    const refName = /^[\u4e00-\u9fa5]{2,5}$/
    const flagName = refName.test(this.data.name)
    if (!flagName) {
      return wx.utils.toast('请输入合法的中文姓名')
    }
    // 2-校验手机号
    const regMobile = /^(0|86|17951)?(13[0-9]|15[012356789]|166|17[3678]|18[0-9]|14[57])[0-9]{8}$/
    var flagMobile = regMobile.test(this.data.mobile)
    if (!flagMobile) {
      return wx.utils.toast('请输入合法的手机号')
    }
    // 3-校验图片是否选择了
    const imgFlag = !!this.data.idcardFrontUrl && !!this.data.idcardBackUrl
    console.log(imgFlag, 777);
    if (!imgFlag) {
      return wx.utils.toast('请上传身份证照片')
    }

    /* 发送请求 */

    // const { point, building, room, name, gender, mobile, idcardFrontUrl, idcardBackUrl } = this.data
    // const result = await wx.http({
    //   url: '/room',
    //   method: 'POST',
    //   data: {
    //     point,
    //     building,
    //     room,
    //     name,
    //     gender,
    //     mobile,
    //     idcardFrontUrl,
    //     idcardBackUrl
    //   }
    // })
    delete this.data.__webviewId__
    delete this.data.status
    await wx.http({
      url: '/room',
      method: 'POST',
      data: this.data
    })
    wx.navigateBack(
      {
        delta: this.data.id ? 2 : 4
      }
    )
    // console.log(result, 666555);

  },



  onLoad ({ id, point, building, room }) {
    console.log(id, point, building, room, 776);
    /* 
    id存在表示是要编辑不存在就是添加
    
    */
    if (id) {
      wx.setNavigationBarTitle({
        title: '编辑房屋信息'
      })
      this.getHouseDetail(id)
      this.setData({ point, building, room, id })  // 下畈新苑 1号楼 下畈新苑1号楼1801 776
    } else {
      this.setData({ point, building, room })  // 下畈新苑 1号楼 下畈新苑1号楼1801 776
    }



  },

  /* 根据id获取详情数据 */
  async getHouseDetail (id) {
    if (!id) return
    // 请求数据接口
    const { code, data: houseDetail } = await wx.http.get('/room/' + id)
    // 检测接口返回的结果
    if (code !== 10000) return wx.utils.toast()
    // 渲染数据
    this.setData({ ...houseDetail })//  data:{gender:1}
  },

})
