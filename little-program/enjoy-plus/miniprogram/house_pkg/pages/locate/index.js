import qqmapsdk from '../../../utils/qqmap'
Page({
  data: {
    address: '',
    points: []
  },
  onLoad () {
    // 获取用户所在位置周围小区
    this.getLocation()
  },
  async getLocation () {
    // 用户所在位置经纬度
    const { latitude, longitude } = await wx.getLocation()
    // 查看经纬度
    console.log(latitude, longitude)
    this.getPoint(latitude, longitude)
  },
  async chooseLocation () {
    const { latitude, longitude } = await wx.chooseLocation()
    // 查看经纬度
    console.log(latitude, longitude, 123)
    this.getPoint(latitude, longitude)
  },
  getPoint (latitude, longitude) {
    console.log(latitude, longitude, 6667);
    qqmapsdk.reverseGeocoder({
      // location: [latitude, longitude].join(','), //获取表单传入的位置坐标,不填默认当前位置,示例为string格式
      location: '30.707141,114.400824', //获取表单传入的位置坐标,不填默认当前位置,示例为string格式
      //get_poi: 1, //是否返回周边POI列表：1.返回；0不返回(默认),非必须参数
      success: (res) => {//成功后的回调
        console.log(res, 88888);
        // 结果为当前所在的地址
        this.setData({ address: res.result.address })
      },
      fail: function (error) {
        console.error(error);
      },
      complete: function (res) {
        console.log(res);
      }
    })

    /* 搜索 */
    qqmapsdk.search({
      keyword: '住宅小区',  //搜索关键词
      // location: [latitude, longitude].join(','),  //设置周边搜索中心点
      location: '30.707141,114.400824',  //设置周边搜索中心点
      success: (res) => { //搜索成功后的回调
        console.log(latitude, longitude, 667);
        var points = res.data.map(item => {
          return {
            id: item.id,
            title: item.title,
            _distance: item._distance
          }
        })
        console.log(points, 5588);
        this.setData({ points })
      },
      fail: function (res) {
        console.log(res);
      },
      complete: function (res) {
        console.log(res);
      }
    });
  },

  /* 跳转到楼栋 */
  goBuilding (ev) {
    console.log(ev, 888);
    /* 跳转地址+传参 */
    wx.navigateTo({
      url: '/house_pkg/pages/building/index?point=' + ev.mark.point
    })
  }
})