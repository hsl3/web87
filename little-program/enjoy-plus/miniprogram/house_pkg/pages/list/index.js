Page({
  onShow () {
    this.getHouseList()
  },
  data: {
    dialogVisible: false,
    houseList: []
  },
  swipeClose ({ mark: { id }, detail: { instance } }) {
    // swipeClose (e) {
    //  只要点击删除按钮就会触发
    // console.log(e, 1111);
    this.setData({ dialogVisible: true, id })
    // /* 2-关闭滑块 */
    instance.close()
  },
  async deleteHouse (id) {
    // 请求数据接口
    const { code } = await wx.http.delete('/room/' + id)
    // console.log(res, 556);
    if (code !== 10000) return wx.utils.toast()
  },
  /* 获取列表 */
  async getHouseList () {
    // 请求数据接口
    const { code, data: houseList } = await wx.http.get('/room')
    // 检测接口返回的结果
    if (code !== 10000) return wx.utils.toast()
    // 渲染数据
    this.setData({ houseList })
  },
  /* 关闭对话框 */
  dialogClose (e) {
    /* 只要是关闭弹出层就会触发 */
    console.log(e, 2222);
    if (e.detail == "confirm") {
      //点击的是确认
      /* 1发送请求 */
      this.deleteHouse(this.data.id)

      // /* 3-更新页面 */
      // /* 方式1：this.getHouseList() */
      // /* 方式2：[1,2,3,4]*/
      var houseList = this.data.houseList.filter(item => {
        return item.id != this.data.id  //
      })
      this.setData({ houseList })
    }
  },
  goDetail ({ mark: { id } }) {
    wx.navigateTo({ url: '/house_pkg/pages/detail/index?id=' + id })
  },
  addHouse () {
    wx.navigateTo({
      url: '/house_pkg/pages/locate/index',
    })
  }
})
