// pages/profile/index.ts
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {
      avatar: '/static/images/avatar_1.jpg'
    }
  },

  getUserNickName (e) {
    // console.log(e, 777);
    this.updateNickName(e.detail.value)
  },

  /*跟心图片 */
  changeAvatar (e) {
    console.log(e.detail.avatarUrl, wx.http.baseURL, 222222);
    // 图片实现上传
    wx.uploadFile({
      url: wx.http.baseURL + '/upload', //仅为示例，非真实的接口地址
      filePath: e.detail.avatarUrl,
      name: 'file',
      header: {
        Authorization: 'Bearer ' + getApp().token
      },
      formData: {
        'type': 'avatar'
      },
      success: (res) => {
        console.log(res, 889999);
        // this.setData({
        //   userInfo: {
        //     avatar: JSON.parse(res.data).data.url
        //   }
        // })
        this.setData({
          'userInfo.avatar': JSON.parse(res.data).data.url
        })
      }
    })
  },

  // 更新昵称
  async updateNickName (nickName) {
    const res = await wx.http.put('/userInfo', {
      nickName
    })

    console.log(res, 999);
  },

  /* 获取用户信息 */
  async getUserInfo () {
    const res = await wx.http.get('/userInfo')
    console.log(res, 777);
    this.setData({
      userInfo: res.data
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad () {
    this.getUserInfo()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage () {

  }
})