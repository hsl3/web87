Page({
  data: {
    notifyList: []
  },
  onLoad () {
    // wx.utils.toast()
    this.getNotifyList()
  },
  async getNotifyList () {
    const { code, data: notifyList } = await wx.http({
      url: '/announcement',
    })
    console.log(code, notifyList, 7777);
    if (code !== 10000) return wx.utils.toast()
    this.setData({
      notifyList: notifyList
    })
  }
})
