const app = getApp()
Page({
  onLoad ({ redirect }) {
    console.log(redirect, 888);
    this.setData({ redirect })
  },
  data: {
    countDownVisible: false,
    redirect: '',
    mobile: "13555555555",
    code: '',
    sendCode: ""
  },

  async start () {

    /*
   1-获取手机号
   2-校验
   3-发送请求===
   4-收集验证码
   */
    /* 2-校验手机号 */
    const reg = /^(0|86|17951)?(13[0-9]|15[012356789]|166|17[3678]|18[0-9]|14[57])[0-9]{8}$/
    var flag = reg.test(this.data.mobile)
    console.log(flag, 8888899);
    if (!flag) {
      return wx.utils.toast('请输入合法的手机号')
    }
    /* 3-发送请求 */
    const res = await wx.http.get('/code', { mobile: this.data.mobile.trim() })
    console.log(res, 88777);
    this.setData({ countDownVisible: true, sendCode: res.data.code })
    //4-收集验证码
    wx.utils.toast(res.data.code)
  },
  /* 表单输入事件 */
  handleInput (e) {
    // 1-获取手机号
    console.log(e.detail, 2222);
    this.setData({ mobile: e.detail })
  },

  // 获取输入的验证码
  handleCode (e) {
    console.log(e.detail, 2222);
    this.setData({ code: e.detail })

  },
  async loginHandle () {
    /*
    1-对比输入的code和获取的 sendCode
    2-比对成功就发送请求否则提示验证码输入错误
    3-登录成功后===获取token并存储=>app+storage
    4-跳转
    */
    // 1-对比输入的code和获取的 sendCode  2-比对成功就发送请求否则提示验证码输入错误
    if (this.data.sendCode !== this.data.code) return wx.utils.toast('验证码输入错误')
    // 3-登录成功后===获取token并存储=>app+storage
    const res = await wx.http.post('/login', { mobile: this.data.mobile.trim(), code: this.data.code })
    // console.log(res, 8999);

    app.token = res.data.token
    app.refreshToken = res.data.refreshToken
    wx.setStorage({
      key: "token",
      data: app.token
    })
    wx.setStorage({
      key: "refreshToken",
      data: app.refreshToken
    })
    // 4-跳转
    wx.redirectTo({
      // url: '/pages/my/index'
      url: this.data.redirect
    })
    // wx.storageSync('token', res.data.token)
    // wx.storageSync('refreshToken', res.data.refreshToken)
    // token赋值+跳转
    // app.token = '1234'
    // wx.setStorage({
    //   key: "token",
    //   data: app.token
    // })
    // wx.redirectTo({
    //   // url: '/pages/my/index'
    //   url: this.data.redirect || '/pages/my/index'
    // })
  },
  countDownChange (ev) {
    console.log(ev, 999);
    this.setData({
      timeData: ev.detail,
      countDownVisible: ev.detail.minutes === 1 || ev.detail.seconds > 0,
    })
  },
})
