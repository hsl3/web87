// map.js
import qqmapsdk from '../../../utils/qqmap'
Page({
  data: {
    latitude: 40.060539,
    longitude: 116.343847,
    dialogVisible: false
  },

  async getRepairDetail (id) {
    // id 不存在就不必发请求了
    if (!id) return
    // 请求数据接口
    const { code, data: repairDetail } = await wx.http.get('/repair/' + id)
    // 校验接口调用结果
    if (code !== 10000) return wx.utils.toast('获取报修详情失败!')
    // 渲染报修详情
    this.setData({ ...repairDetail })
    this.getMap(repairDetail.houseInfo)
  },

  /* 地图规划 */


  getMap (e) {
    var _this = this;
    console.log(e, 8877);
    //调用地址解析接口
    qqmapsdk.getSuggestion({
      //获取表单传入地址
      keyword: e, //地址参数，例：固定地址，address: '北京市海淀区彩和坊路海淀西大街74号'
      success: async (res) => {//成功后的回调
        // console.log(res.data[0].location, 9999);
        const { lat, lng } = res.data[0].location//房屋坐标
        const { latitude: myLatitude, longitude: myLongitude } = await wx.getLocation()// 获取当前所在坐标
        /* 规划路线 */
        qqmapsdk.direction({
          mode: 'driving',//可选值：'driving'（驾车）、'walking'（步行）、'bicycling'（骑行），不填默认：'driving',可不填
          //from参数不填默认当前地址
          from: [myLatitude, myLongitude].join(','),//当前所在位置
          to: [lat, lng].join(','),//  目的地 其实就是维修点的坐标
          success: function (res) {//成功后的回调
            // console.log(res, 9999);
            var ret = res;
            var coors = ret.result.routes[0].polyline, pl = [];
            //坐标解压（返回的点串坐标，通过前向差分进行压缩）
            var kr = 1000000;
            for (var i = 2; i < coors.length; i++) {
              coors[i] = Number(coors[i - 2]) + Number(coors[i]) / kr;
            }
            //将解压后的坐标放入点串数组pl中
            for (var i = 0; i < coors.length; i += 2) {
              pl.push({ latitude: coors[i], longitude: coors[i + 1] })
            }
            console.log(pl)
            //设置polyline属性，将路线显示出来,将解压坐标第一个数据作为起点
            _this.setData({
              latitude: pl[0].latitude,
              longitude: pl[0].longitude,
              polyline: [{
                points: pl,
                color: '#FF0000DD',
                width: 4
              }]
            })

          },
          fail: function (error) {
            console.error(error);
          },
          complete: function (res) {
            console.log(res);
          }
        })
      },
      fail: function (error) {
        console.error(error);
      },
      complete: function (res) {
        console.log(res);
      }
    })
  },

  editRepair () {
    wx.navigateTo({
      // repair_id 是全局变量，在前面已经定义
      url: '/repair_pkg/pages/form/index?id=' + this.data.id,
    })
  },

  onLoad ({ id }) {
    console.log(id, 99);
    this.getRepairDetail(id)
    this.setData({ id })
  },
  dialogClose (e) {
    console.log(e, 232222);
    if (e.detail == "confirm") {
      //  点击确认
      this.cancelRepair()

    }
    // ...
  },
  openDialogLayer () {
    this.setData({
      dialogVisible: true
    })
  },
  async cancelRepair () {
    // 请求数据接口
    const { code } = await wx.http.put('/cancel/repaire/' + this.data.id)
    // 检测接口的调用结果
    if (code !== 10000) return wx.utils.toast('取消报修失败!')
    // 跳转到报修列表页面
    wx.navigateBack()
  },

})
