Page({
  data: {
    appointment: '',
    houseInfo: '请选择报修房屋',
    repairItemName: "选择维修项目",
    currentDate: '请选择时间',
    minDate: new Date().getTime(),

    houseLayerVisible: false,
    repairLayerVisible: false,
    dateLayerVisible: false,
    houseList: [],
    repairItem: [],
    mobile: '10086',
    description: '',
    fileList: [
      { url: '/repair_pkg/static/uploads/attachment.jpg' },
      { url: '/repair_pkg/static/uploads/attachment.jpg' },
    ],
  },

  openHouseLayer () {
    this.setData({ houseLayerVisible: true })
  },
  closeHouseLayer () {
    this.setData({ houseLayerVisible: false })
  },
  openRepairLayer () {
    this.setData({ repairLayerVisible: true })
  },
  closeRepairLayer () {
    this.setData({
      repairLayerVisible: false,
    })
  },

  openDateLayer () {
    this.setData({ dateLayerVisible: true })
  },
  closeDateLayer () {
    this.setData({ dateLayerVisible: false })
  },
  goList () {
    wx.reLaunch({
      url: '/repair_pkg/pages/list/index',
    })
  },
  /* 选择报修房屋 */
  onSelect (event) {
    console.log(event, 222333);
    // 获取用户选择房屋的名称
    const { name: houseInfo, id: houseId } = event.detail
    // 页面中渲染
    this.setData({ houseInfo, houseId })
  },
  /* 选择维修项目 */
  selectRepairItem (event) {
    console.log(event, 4444);
    // 获取用户选择房屋的名称
    const { name: repairItemName, id: repairItemId } = event.detail
    // 页面中渲染
    this.setData({ repairItemName, repairItemId })
  },
  /* 请选择上门维修日期 */
  selectDate ({ detail }) {
    console.log(detail, 7777);
    this.setData({
      appointment: wx.formatDate(detail),
    });

    // this.setData({
    //   currentDate: event.detail,
    // });
    // console.log(event);
    this.closeDateLayer()
  },

  async getHouseList () {
    // 请求数据接口
    const { code, data: houseList } = await wx.http.get('/house')
    // 检测接口返回的结果
    if (code !== 10000) return wx.utils.toast('获取房屋列表失败!')
    // 数据渲染
    this.setData({ houseList })
  },
  async getRepairItem () {
    // 请求数据接口
    const { code, data: repairItem } = await wx.http.get('/repairItem')
    // 检测接口返回的数据
    if (code !== 10000) return wx.utils.toast('获取维修项目失败!')
    // 数据渲染
    this.setData({ repairItem })
  },

  /* 图片上传 */
  afterRead (ev) {
    console.log(ev, 88)
    // 临时文件
    const { file } = ev.detail
    // 调用接口上传至服务器
    wx.uploadFile({
      url: wx.http.baseURL + '/upload',
      filePath: file.url,
      name: 'file',
      header: {
        Authorization: "Bearer " + getApp().token,
      },
      success: (res) => {
        console.log(res, 6677);
        // 转换 json 数据
        const data = JSON.parse(res.data)
        // 上传完成需要更新
        // 方法1：
        const { attachment = [] } = this.data
        attachment.push({ ...data.data })//[{id,url}]
        // 方法2：
        // this.data.attachment.push(data.data)
        // this.setData({ attachment:[...this.data.attachment], fileList: attachment })
        // 方法3：
        //  this.setData({ attachment:[...this.data.attachment,data.data], fileList: attachment })
        // // 渲染数据
        this.setData({ attachment, fileList: attachment })
      },
    })
  },

  verifyHouse () {
    const valid = this.data.houseId !== ''
    // 验证结果提示
    if (!valid) wx.utils.toast('请选择房屋信息!')
    // 返回验证结果
    return valid
  },
  verifyRepair () {
    const valid = this.data.repairItemId !== ''
    // 验证结果提示
    if (!valid) wx.utils.toast('请选择维修项目!')
    // 返回验证结果
    return valid
  },
  verifyMobile () {
    // 验证手机号
    const reg = /^[1][3-8][0-9]{9}$/
    const valid = reg.test(this.data.mobile.trim())
    // 验证结果提示
    if (!valid) wx.utils.toast('请填写正确的手机号码!')
    // 返回验证结果
    return valid
  },
  verifyDate () {
    // 验证日期格式
    const reg = /^\d{4}\/\d{2}\/\d{2}$/
    const valid = reg.test(this.data.appointment)
    // 验证结果提示
    if (!valid) wx.utils.toast('请选择预约日期!')
    // 返回验证结果
    return valid
  },
  verifyDescription () {
    // 验证报修项目描述
    const valid = this.data.description.trim() !== ''
    // 验证结果提示
    if (!valid) wx.utils.toast('请填写问题描述!')
    // 返回验证结果
    return valid
  },

  /* 提交检修 */
  async submitForm () {
    // if (!this.verifyHouse()) return
    // if (!this.verifyRepair()) return
    // if (!this.verifyMobile()) return
    // if (!this.verifyDate()) return
    // if (!this.verifyDescription()) return

    const { id, houseId, repairItemId, appointment, mobile, description, attachment } = this.data
    const { code } = await wx.http.post('/repair', {
      id, houseId, repairItemId, appointment, mobile, description, attachment
    })
    // console.log(res, 777);
    if (code !== 10000) return wx.utils.toast('提交维修失败')
    wx.navigateTo({ url: '/repair_pkg/pages/list/index' })

  },

  /* 报修修改 */
  async getRepairDetail (id) {
    if (!id) return
    // 请求数据接口
    const { code, data: repairDetail } = await wx.http.get('/repair/' + id)
    // 检测接口调用结果
    if (code !== 10000) return wx.utils.toast('获取报修信息失败!')
    // 渲染报修信息
    this.setData({ ...repairDetail, fileList: repairDetail.attachment })
  },


  onLoad ({ id }) {
    /* id:修改报修没有id表示添加报修 */
    // 获取房屋列表
    this.getHouseList()
    this.getRepairItem()

    if (id) {
      wx.setNavigationBarTitle({
        title: '编辑报修信息'
      })
      // this.setData({ id })
      // 发送请求获取数据进行数据回显
      this.getRepairDetail(id)
    }
  },
})
