function formatDate(unixtime) {
  var date = new Date(unixtime)
  var y = date.getFullYear();
  var m = date.getMonth() + 1;
  m = m < 10 ? ('0' + m) : m;
  var d = date.getDate();
  d = d < 10 ? ('0' + d) : d;
  // var h = date.getHours();
  // h = h < 10 ? ('0' + h) : h;
  // var minute = date.getMinutes();
  // var second = date.getSeconds();
  // minute = minute < 10 ? ('0' + minute) : minute;
  // second = second < 10 ? ('0' + second) : second;
  // return y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second;//年月日时分秒
  // return y + '-' + m + '-' + d + ' ' + h + ':' + minute;
  // return y + '-' + m + '-' + d;
  return y + '-' + m + '-' + d;
}

/**
 * 扩展 wx 全局对象，切记不要与原用 api 重名
 */
wx.formatDate = formatDate


//  wx.utils.toast()

/**
 * 模块化导出
 */
export default formatDate