// 导入 http 模块
import http from 'wechat-http'
/**
 * 配置接口基础路径
 */

http.baseURL = 'https://live-api.itheima.net'
/**
 * 挂载方法到全局
 */
/**
 * 配置响应拦截器
 */
/*
 *
 * 配置响应拦截器
 */
http.intercept.request = (params) => {
  // console.log(params, 9988);
  const { token } = getApp()
  // // 这里必须要有返回
  // /* 判断有没有token有家设置请求头 */
  const defaultHeader = {}
  // 追加 token 头信息
  if (token) defaultHeader.Authorization = 'Bearer ' + token
  // 合并自定义头信息和公共头信息   浅拷贝 Object.assign(原先的数据,新的数据)====新的数据覆盖原先的数据
  /* 合并 */
  params.header = Object.assign(defaultHeader, params.header)  //  刷新token
  return params
  // if (token) {
  //   // params.header.Authorization = 'Bearer ' + token
  //   params.header = {
  //     Authorization: 'Bearer ' + token
  //   }
  // }
  // return params
}

http.intercept.response = async ({ statusCode, data, config }) => {

  if (statusCode === 401) {
    //  token过期
    const app = getApp()
    /* 判断refreshToken是否过期 */
    console.log(config, 2222);
    if (config.url.includes('/refreshToken')) {
      console.log(config, 33333);
      app.token = ''
      app.refreshToken = ''
      wx.clearStorageSync()
      var redirect = getCurrentPages()[getCurrentPages().length - 1].route
      wx.redirectTo({ url: '/pages/login/index?redirect=/' + redirect })
      return
    }

    // console.log(wx.getStorageSync('refreshToken'), 66666);
    const res = await http({
      url: '/refreshToken',
      method: 'POST',
      header: {
        // 这时要注意使用的是 refresh_token
        Authorization: 'Bearer ' + wx.getStorageSync('refreshToken'),
      },
    })
    console.log(res, 777666);// 返回新的token和refreshToken
    app.token = res.data.token
    app.refreshToken = res.data.refreshToken
    wx.setStorage({
      key: "token",
      data: res.data.token
    })
    wx.setStorage({
      key: "refreshToken",
      data: res.data.refreshToken
    })
    // 重新发起请求
    // console.log(config, 8888);
    return http(
      Object.assign(config, {
        // 传递新的 token
        header: {
          Authorization: 'Bearer ' + wx.getStorageSync('token'),
        },
      })
    )
  }
  // 过滤接口返回的数据
  // console.log(config, app.token, 9999);
  return data
}
wx.http = http
/**
 * 模块化导出
 */
export default http