// components/footers/footers.js
Component({
  options: {
    // 启用多插槽支持
    multipleSlots: true
  },
  /**
   * 组件的属性列表  props
   */
  properties: {
    name:{
      type:String,
      value:'rose'  //  value===default
    },
    nickName:String
  },
  /**
   * 组件的初始数据
   */
  data: {
    n:1
  },
  /**
   * 组件的方法列表
   */
  methods: {
    btn(){
      // vue----this.$emit('事件名',值)
      console.log(this,1111);
      // triggerEvent===$emit
      this.triggerEvent('son','我是footers组件')
    }
  },
  // 生命周期
  lifetimes:{
    created (){
      console.log(this.data.n,888);
      this.setData({
        n:2
      })
      this.author = 'itcast';
      console.log(this,888);
    },
    attached(){
      //  mounted  
      //  以后获取数据发送请求在这个阶段执行
      // 获取了数据将数据渲染到页面上实时展示需要在这个阶段执行
      this.setData({
        n:2
      })
    }
  }
})
