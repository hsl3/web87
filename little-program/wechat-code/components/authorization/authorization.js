// components/authorization/authorization.js
Component({
  options: {
    // 启用多插槽支持
    multipleSlots: true
  },
  /**
   * 组件的属性列表
   */
  properties: {
    isLogin:Boolean,
    tips:String
  },

  /**
   * 组件的初始数据
   */
  data: {
    message: '组件中初始的数据',
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
