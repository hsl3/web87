// pages/second/second.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    n: 4,
    msg: 'hello  world',
    arr: [1, 2, 3],
    obj: {
      id: 1,
      name: 'jack'
    },
    flag: true,
    list: [{
        id: 1,
        name: 'jack'
      },
      {
        id: 2,
        name: 'rose'
      },
      {
        id: 3,
        name: 'tom'
      }
    ],
    studentsList:[]
  },
  btn() {
    this.setData({
      flag: !this.data.flag
    })
  },

  // 事件处理函数
  handle() {
    console.log(this, 1111);
    console.log(this.data.msg, 2222);
    // this.data.msg='hello  小程序' //  数据可以更改  但是视图不更新
    // console.log(this.data.msg,3333);
    this.setData({
      msg: 'hello  小程序',
      n: this.data.n + 1
    })
  },
  handle2() {
    console.log(1111);
  },
  handle3(e) {
    console.log(e.detail.value, 4444);
    this.setData({
      n: e.detail.value
    })

  },
  handle4(e){
    console.log(e.target.dataset,'handle4');
  },
  handle5(){
    // 编程式导航   this.$router.push
    // 跳转到非tabBar页面
    wx.navigateTo({
      url:'/pages/forth/forth?id=123&name=jack'
    })
    // 跳转到tabBar页面
    // wx.switchTab({
    //   url:'/pages/third/third'
    // })
  },
  handle6(){
    wx.showLoading({
      title:'获取数据中',
      // icon:'loading'
    })
   setTimeout(()=>{
    wx.request({
      url:'https://mock.boxuegu.com/mock/3293/students',
      success:(res)=>{
        console.log(res,6666);
        if(res.data.code==10000){
          this.setData({
            studentsList:res.data.result
          })
          wx.showToast({
            title:'获取数据成功'
          })
        }
       
      }
    })
   },2000)
  },

  handle7(){
    wx.showModal({
      title:'删除',
      content:'您确定要删除数据嘛',
      // showCancel:false
      cancelText:'退出',
      success:(res)=>{
        console.log(res,888);
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log('页面====onLoad 监听页面加载，只会执行 1 次');
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    console.log('页面===onReady==监听页面初次渲染完成');
  },


  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    console.log('页面===onShow==监听页面显示');
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    console.log('页面===onHide');
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})