// index.js
// 获取应用实例
const app = getApp()
import myBehavior from './my-behavior'
Page({
  behaviors: [myBehavior],
  data: {
    list:[
      {id:1,img:'/assets/head.jpg',chk:true,num:2,price:100,title:'我是11111111111111111111一个标题~~~~~'},
      {id:2,img:'/assets/head.jpg',chk:true,num:12,price:100,title:'我是一个标题~~~~~'},
      {id:3,img:'/assets/head.jpg',chk:true,num:22,price:100,title:'我是一个标题~~~~~'},
      {id:4,img:'/assets/head.jpg',chk:true,num:32,price:300,title:'我是一个标题~~~~~'},
      {id:5,img:'/assets/head.jpg',chk:true,num:42,price:400,title:'我是一个标题~~~~~'},
    ],
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    canIUseGetUserProfile: false,
    canIUseOpenData: wx.canIUse('open-data.type.userAvatarUrl') && wx.canIUse('open-data.type.userNickName') // 如需尝试获取用户信息可改为false
  },
  // 事件处理函数
  bindViewTap() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad() {
    console.log(this,5555);
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
  },
  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res)
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    })
  },
  getUserInfo(e) {
    // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
    console.log(e)
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})
