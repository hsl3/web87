// pages/third/third.js
const computed=require('miniprogram-computed')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    profile: {
      avatarUrl: '/assets/bigUserHeader.png',
      nickName: '微信用户',
    },
    userInfo:{},
    arr:[],
    falg:false,
    scrollFlag:false,
    tempFilePath:''
  },
  sonHandle(e){
    console.log(e.detail,'sonHandle');
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    wx.vibrateShort()
    // console.log(options,88);

  },
  handle() {
    console.log(111);
    // 同步
    // wx.setStorageSync('user',{id:1,name:'jack'})
    // 异步
    wx.setStorage({
      key: 'user',
      data: {
        id: 2,
        name: 'rose'
      }
    })
  },
  handle2() {
    // 同步
    //  console.log( wx.getStorageSync('user'));
    // 异步
    wx.getStorage({
      key: 'user',
      success(res) {
        console.log(res)
      }
    })
  },
  handle3() {
    wx.removeStorage({
      key: 'user',
    })
  },
  getUserProfile() {
    // console.log(1111222);
    // 推荐使用 wx.getUserProfile 获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认
    // 开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res,7777);
        this.setData({
          userInfo: res.userInfo,
        })
      }
    })
    // wx.getUserInfo({
    //   success: function(res) {
    //     console.log(res,99988);
    //     var userInfo = res.userInfo
    //     var nickName = userInfo.nickName
    //     var avatarUrl = userInfo.avatarUrl
    //     var gender = userInfo.gender //性别 0：未知、1：男、2：女
    //     var province = userInfo.province
    //     var city = userInfo.city
    //     var country = userInfo.country
    //   }
    // })
  },

  handle4() {
    console.log(4444);
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success:(res)=> {
        // tempFilePath可以作为 img 标签的 src 属性显示图片
        const tempFilePaths = res.tempFilePaths
        console.log(tempFilePaths);
        // 将临时获取的图片路径上传到服务器
        // 上传临时文件
        wx.uploadFile({
          url: 'http://ajax-api.itheima.net/api/file',
          filePath: tempFilePaths[0],
          name: 'avatar',
          success: (res) => {
            console.log(res,999);
            this.setData({
              profile:{
                avatarUrl:JSON.parse(res.data).data.url ,
                nickName: '微信用户',
              }
            })
          },
        });
      }

    })
  },
  scan(){
    wx.scanCode()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    console.log('onPullDownRefresh');
    this.setData({
      arr:[]
    })
    this.handle6()
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    console.log('onReachBottom');
    // if(this.data.arr.length>=50){
    //   return 
    // }
    // var  newData=[1,2,3,4,5,6,7,8,9,10]
    // this.setData({
    //   arr:[...this.data.arr,...newData]
    // })
   
    this.handle6()
  },
  handle6(){
    if(this.data.arr.length>=20){
      this.setData({
        falg:true
      })
      return 
    }
    wx.showLoading({
      title:'获取数据中',
      // icon:'loading'
    })
   setTimeout(()=>{
    wx.request({
      url:'https://mock.boxuegu.com/mock/3293/students',
      success:(res)=>{
        console.log(res,6666);
        if(res.data.code==10000){
          this.setData({
            arr:[...this.data.arr,...res.data.result]
          })
          wx.showToast({
            title:'获取数据成功'
          })
        }
      }
    })
   },2000)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  // 页面滚动就触发
  onPageScroll({scrollTop}){
    console.log(scrollTop);
    if(scrollTop>30){
      this.setData({
        scrollFlag:true
      })
    }else{
      this.setData({
        scrollFlag:false
      })
    }
  },
  goTop(){
    wx.pageScrollTo({
      scrollTop: 0,
      duration: 300
    })
  },

  start(){
    //  开始录音
    wx.startRecord({
      success :(res)=> {
        console.log(res,888);
        const tempFilePath = res.tempFilePath
        this.setData({
          tempFilePath
        })
      }
    })
  },
  stop(){
    wx.stopRecord() // 结束录音
  },
  play(){
    wx.playVoice({
      filePath: this.data.tempFilePath,
    })
  }
})