// app.js
App({
  onLaunch(params) {
    console.log('应用===onLaunch');
    // 展示本地存储能力
    const logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
  },
  onShow(){
    // console.log('应用===onShow');
  },
  onHide(){
    // console.log('应用===onHide');
  },
  globalData: {
    userInfo: null
  },
  token:'',
  http(params) {
    // 举例封装网络请求
    wx.request({
      ...params,
      header: {},
    })
  }
})
