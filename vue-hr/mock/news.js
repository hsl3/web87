module.exports = [
  {
    url: '/news/list',
    type: 'get',
    response: config => {
      return {
        message: 'OK',
        code: 20000,
        data: ['list1', 'list2']
      }
    }
  }
]
