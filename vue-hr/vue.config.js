'use strict'
const path = require('path')
const defaultSettings = require('./src/settings.js')

function resolve (dir) {
  return path.join(__dirname, dir)
}

const name = defaultSettings.title || 'vue Admin Template' // page title

// If your port is set to 80,
// use administrator privileges to execute the command line.
// For example, Mac: sudo npm run
// You can change the port by the following methods:
// port = 9528 npm run dev OR npm run dev --port = 9528
// port: 端口
// process 是nodejs的全局变量
// env: environment 环境
const port = process.env.port || process.env.npm_config_port || 9528 // dev port

console.log(process.env.NODE_ENV, 88888)
// All configuration item explanations can be find in https://cli.vuejs.org/config/

var externals = {}
let cdn = { css: [], js: [] }
if (process.env.NODE_ENV === 'production') {
  externals = {
    /**
   * externals 对象属性解析。
   *  基本格式：
   *     '包名' : '在项目中引入的名字'
   *
 */
    'vue': 'Vue',
    'element-ui': 'ELEMENT', // ELEMENT
    'xlsx': 'XLSX'
  }

  cdn = {
    css: [
      'https://unpkg.com/element-ui/lib/theme-chalk/index.css' // element-ui css 样式表
    ],
    js: [
      // vue must at first!
      'https://unpkg.com/vue@2.6.12/dist/vue.js', // vuejs
      'https://unpkg.com/element-ui/lib/index.js', // element-ui js
      'https://cdn.jsdelivr.net/npm/xlsx@0.16.6/dist/xlsx.full.min.js' // xlsx
    ]
  }
} else {
  externals = {}
}
module.exports = {
  /**
   * You will need to set publicPath if you plan to deploy your site under a sub path,
   * for example GitHub Pages. If you plan to deploy your site to https://foo.github.io/bar/,
   * then publicPath should be set to "/bar/".
   * In most cases please use '/' !!!
   * Detail: https://cli.vuejs.org/config/#publicpath
   */
  publicPath: './',
  outputDir: 'dist',
  assetsDir: 'static',
  lintOnSave: process.env.NODE_ENV === 'development',
  productionSourceMap: false,
  devServer: {
    port: port,
    open: true,
    overlay: {
      warnings: false,
      errors: true
    },
    proxy: {
      /* 前端配置代理服务器：只在开发环境会生效，上线生产环境就不会生效了 */
      // 如果请求的路径是api打头，就转发
      // 例如：
      // http://192.168.81.22:9528/api/abc/efg  =====> http://ihrm-java.itheima.net/api/abc/efg
      '/api': {
        // target: 'http://ihrm-java.itheima.net' // 接口服务器
        target: 'http://ihrm-java.itheima.net'
        // target: 'http://192.168.81.211:3000'
      }
    }
  },
  configureWebpack: {
    /* 打包排除哪些包  表示这里定义的包不会被打包了
    开发环境：development【npm run dev】  本地开发的时候不需要执行需要这些包
    生产环境：production【npm  run  build:prod】  排除项只能走上线的时候执行

    **** 排除项只能走上线的时候执行 本地开发的时候不需要执行需要这些包
    */
    //   externals: {
    //     /**
    //    * externals 对象属性解析。
    //    *  基本格式：
    //    *     '包名' : '在项目中引入的名字'
    //    *
    //  */
    //     'vue': 'Vue',
    //     'element-ui': 'ElementUI',
    //     'xlsx': 'XLSX'
    //   },
    externals,
    // provide the app's title in webpack's name field, so that
    // it can be accessed in index.html to inject the correct title.
    name: name,
    resolve: {
      alias: {
        '@': resolve('src')
      }
    }
  },
  chainWebpack (config) {
    /* 打包上线去除console */
    config.optimization.minimizer('terser').tap((args) => {
      args[0].terserOptions.compress.drop_console = true
      return args
    })
    /* 注入cdn在html中使用 */
    // 注入cdn变量 (打包时会执行)
    config.plugin('html').tap(args => {
      args[0].cdn = cdn // 配置cdn给插件
      return args
    })
    // it can improve the speed of the first screen, it is recommended to turn on preload
    config.plugin('preload').tap(() => [
      {
        rel: 'preload',
        // to ignore runtime.js
        // https://github.com/vuejs/vue-cli/blob/dev/packages/@vue/cli-service/lib/config/app.js#L171
        fileBlacklist: [/\.map$/, /hot-update\.js$/, /runtime\..*\.js$/],
        include: 'initial'
      }
    ])

    // when there are many pages, it will cause too many meaningless requests
    config.plugins.delete('prefetch')

    // set svg-sprite-loader
    config.module
      .rule('svg')
      .exclude.add(resolve('src/icons'))
      .end()
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end()

    config
      .when(process.env.NODE_ENV !== 'development',
        config => {
          config
            .plugin('ScriptExtHtmlWebpackPlugin')
            .after('html')
            .use('script-ext-html-webpack-plugin', [{
              // `runtime` must same as runtimeChunk name. default is `runtime`
              inline: /runtime\..*\.js$/
            }])
            .end()
          config
            .optimization.splitChunks({
              chunks: 'all',
              cacheGroups: {
                libs: {
                  name: 'chunk-libs',
                  test: /[\\/]node_modules[\\/]/,
                  priority: 10,
                  chunks: 'initial' // only package third parties that are initially dependent
                },
                elementUI: {
                  name: 'chunk-elementUI', // split elementUI into a single package
                  priority: 20, // the weight needs to be larger than libs and app or it will be packaged into libs or app
                  test: /[\\/]node_modules[\\/]_?element-ui(.*)/ // in order to adapt to cnpm
                },
                commons: {
                  name: 'chunk-commons',
                  test: resolve('src/components'), // can customize your rules
                  minChunks: 3, //  minimum common number
                  priority: 5,
                  reuseExistingChunk: true
                }
              }
            })
          // https:// webpack.js.org/configuration/optimization/#optimizationruntimechunk
          config.optimization.runtimeChunk('single')
        }
      )
  }
}
