// 获取部门列表信息
import request from '@/utils/request.js'
export function getDepartments() {
  return request({
    url: '/company/department',
    method: 'GET'
  })
}

// 查询 员工信息
// 封装查询部门列表  渲染弹框下拉菜单
export function gitEmployees() {
  return request({
    url: '/sys/user/simple',
    method: 'GET'
  })
}

// 新增 封装新增部门接口
export function addDepartments(data) {
  return request({
    url: '/company/department',
    method: 'post',
    data
  })
}
// 根据id查询当前部门详情（数据回填的时候用）
export function getDepartDetail(id) {
  return request({
    url: `/company/department/${id}`,
    method: 'get'
  })
}
// 封装更新部门详情接口  编辑修改
export function updateDepartments(data) {
  return request({
    url: `/company/department/${data.id}`,
    method: 'put',
    data
  })
}

// 根据id删除当前部门
export function DeleteDepartDetail(id) {
  return request({
    url: `/company/department/${id}`,
    method: 'delete'
  })
}

