
import request from '@/utils/request'
// 获取所有角色列表
export function getRoles(page, pagesize) {
  return request({
    url: '/sys/role',
    method: 'get',
    params: { page, pagesize }
  })
}

// 删除   根据id删除当前角色
export function DelRoles(id) {
  return request({
    url: `/sys/role/${id}`,
    method: 'delete'
  })
}
// 新增 角色
export function addRole(data) {
  return request({
    url: '/sys/role',
    method: 'post',
    data
  })
}

//  编辑 数据回填  data{id  description  name}
export function updateRole(data) {
  return request({
    url: `/sys/role/${data.id}`,
    method: 'put',
    data
  })
}
// 根据id获取角色详情
export function getRoleDetail(id) {
  return request({
    url: `/sys/role/${id}`
  })
}

/**
 * 给角色分配权限
 * @param {*} data {id:角色id, permIds:[] 所有选中的节点的id组成的数组}
 * @returns
 */
export function assignPerm(data) {
  return request({
    url: '/sys/role/assignPrem',
    method: 'put',
    data
  })
}

