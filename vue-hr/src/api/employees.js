import request from '@/utils/request'

// 员工列表
export function employeeList(page, size) {
  return request({
    url: '/sys/user',
    method: 'get',
    params: {
      page,
      size
    }
  })
}

// 删除员工
export function DelRoles(id) {
  return request({
    method: 'delete',
    url: `/sys/user/${id}`
  })
}
// 新增员工
export function addEmployee(data) {
  return request({
    method: 'post',
    url: '/sys/user',
    data
  })
}

// 批量导入接口
export function importEmployeeExcel(data) {
  return request({
    url: '/sys/user/batch',
    method: 'post',
    data
  })
}

// 分配角色
export function getRolesId(data) {
  return request({
    url: '/sys/user/assignRoles',
    method: 'put',
    data
  })
}
