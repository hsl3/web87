import request from '@/utils/request.js'
// 1 登录接口
export function userLogin(data) {
  return request({
    url: '/sys/login',
    method: 'POST',
    data: data
  })
}
//  2 获取用户信息接口
export function userInfo() {
  // 这里的请求一定要带token 在请求头中添加token
  return request({
    url: '/sys/profile',
    method: 'POST'
  })
}
// 获取个人信息
export function getUserDetailById(id) {
  return request({
    url: `/sys/user/${id}`
  })
}

// 保存员工修改后的信息 员工头像
export function saveUserDetail(data) {
  return request({
    url: `/sys/user/${data.id}`,
    method: 'put', // 全量修改
    data
  })
}
