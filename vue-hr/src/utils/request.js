
import router from '@/router'
import store from '@/store'
import axios from 'axios'
const request = axios.create({
  // baseURL: 'http://ihrm-java.itheima.net',
  // baseURL: process.env.VUE_APP_BASE_API,
  baseURL: '/api',
  timeout: 5000
})

// 响应拦截器
import { Message } from 'element-ui'
request.interceptors.response.use(response => {
  // console.log(response.data)
  if (response.data.success === false) {
    Message.error(response.data.message)
    return Promise.reject(new Error(response.data.message))
  } else {
    return response.data
  }
},
  async error => {
    // alert('111') 在这里处理错误
    // console.dir(error) // for debug
    if (error.response.status === 401) {
      // 清空信息，类似于用户退出
      await store.dispatch('user/logout')
      // router.push('/login') //跳转到登录页
      // router.push('/login?redirect=' + router.currentRoute.fullPath)
      router.push(`/login?redirect=${router.currentRoute.fullPath}`) // bug
    }
    Message({
      message: error.response.data.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

// 请求拦截器 除了登录 其他页面都需要token
// 在请求头中带上token
request.interceptors.request.use(config => {
  // 从store中去取token
  // console.log(store)
  const token = store.state.user.token
  // 如果token为true 也就是如果有token就在请求头中添加token 如果没有token就返回错误信息
  if (token) {
    config.headers.Authorization = `Bearer ${token}` // 在请求头中添加token
  }

  // console.log('config', config.headers)
  return config
}, err => {
  return Promise.reject(err)
})
export default request
