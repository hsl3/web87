import Cookies from 'js-cookie' // 将token放在cokies里面

const TokenKey = 'vue_admin_template_token'
// 封装获取token的函数 将token存储在cookies中
export function getToken() {
  return Cookies.get(TokenKey)
}

// 存储token
export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

// 删除token
export function removeToken() {
  return Cookies.remove(TokenKey)
}
