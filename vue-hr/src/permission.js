
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css'
// 导入路由
import router, { asyncRouter } from '@/router'
// 导入vuex
import store from '@/store'
import getPageTitle from '@/utils/get-page-title'
// 路由导航守卫： 我们一般把路由的导航守卫拿出来，单独写一个permission.js文件用来做鉴权
// 如果是登录用户：访问/login 放行跳转到主页next('/')  访问其他页面也放行(有token)
// 如果不是的登录用户（定义一个白名单 whileList）：访问/login 或者访问404页面 就放行   访问其他页面不放行(没有token) 跳转到登录页
// whileList：白名单 未登录可以访问的页面
// import { userInfo } from '@/api/user'
// 路由前置守卫
const whileList = ['/login', '/404']
router.beforeEach(async (to, from, next) => {
  // console.log(to.meta.title)
  // 动态设置标题
  // document.title = '人资' + to.meta.title
  document.title = getPageTitle(to.meta.title)
  NProgress.start() // 启动进度条
  const token = store.state.user.token
  // console.log(token)
  if (token) {
    if (to.path === '/login') {
      next('/')
      NProgress.start()
    } else {
      // 判断是否有userId  如果有就放行 没有就先获取个人信息再放行
      next()
      if (!store.state.user.userInfo.userId) {
        // // 发请求拿到用户信息 保存到vuex
        // await store.dispatch('user/userprofile') // 只要用户一登录进来 获取用户信息

        // 获取用户信息=====menu
        const menus = await store.dispatch('user/userprofile')
        // console.log(menus, 959595)
        // 筛选出动态路由规则
        const newArr = asyncRouter.filter(route => {
          // console.log(route, 111111111)
          return menus.includes(route.children[0].name)
        })
        newArr.push({ path: '*', redirect: '/404', hidden: true })
        // 在这里动态添加路由规则解决了：可以在浏览器上输入路由值访问页面
        // router.addRoutes(asyncRouter) // 动态添加8个路由规则，左侧菜单不显示
        router.addRoutes(newArr)
        // 将动态的权限存在vuex的menu模块中
        // this.$store.commit() 这里没有this 导入store 直接访问
        // store.commit('menu/setMenu', asyncRouter) // 显示左侧菜单
        store.commit('menu/setMenu', newArr) // 显示左侧菜单
        // 解决动态添加的路由规则因为放行的时候有可能没有找到，所以需要重新再来执行一次路由规则
        // next(to.path)

        // 当前页面退出登录以后 记录路径 下次登录进来跳转到退出时的路径  遗留的问题 下次换账号登录没有这个权限 将会看到404页面
        //  判断我跳转的地址在不在当前登录用户的角色权限里面  如果在 就可以正常跳转 不在 就跳转到首页
        // .substring(1) 去掉字符串的第一个/
        console.log(menus, to.path.split('/')[1], 666);
        if (menus.includes(to.path.split('/')[1])) {
          next({ path: to.path, replace: true }) // replace:true不留历史痕迹
        } else {
          console.log(2222);
          next('/')
        }
      } else {
        next()
      }
    }
  } else {
    if (whileList.includes(to.path)) {
      next()
    } else {
      next('/login')
      NProgress.done() // 结束进度条
    }
  }
})
// 路由后置守卫
router.afterEach((to, from, next) => {
  NProgress.done() // 结束进度条
})
