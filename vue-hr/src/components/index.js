//  定义全局组件的格式
// import 组件对象 from 'xxxxx.vue'
// Vue.component('组件名', 组件对象)
import store from '@/store'
import pageTools from '@/components/PageTools/index.vue'
import UploadImg from '@/components/UploadImg' // 导入组件
import ImageHolder from '@/components/ImageHolder'
import Lang from '@/components/lang'
import ScreenFull from './ScreenFull'
// Vue.use()的作用：注册插件，增强Vue的功能
// 格式：
// Vue.use(对象)
// 对象中有一个install函数，Vue.use(对象)就调用install函数
// Vue.use(install(vue){
// })

export default {
  install: function(Vue) {
    Vue.prototype.fn = function() {
      alert('fn')
    }
    Vue.component('pageTools', pageTools)
    Vue.component('UploadImg', UploadImg) // 注册全局组件
    Vue.component('ImageHolder', ImageHolder)// 注册全局组件
    Vue.component('Lang', Lang)// 注册全局组件
    Vue.component('ScreenFull', ScreenFull)// 注册全局组件
    // 权限按钮
    Vue.directive('allow', {
      inserted(el, binding, value) {
        // console.log(el, binding, value, 9999999999)
        // console.log(store, 22222222222222)
        const point = store.state.user.userInfo.roles.points
        const flag = point.includes(binding.value)
        if (!flag) {
          // el.style.display = 'none' 按钮影藏
          el.parentNode.removeChild(el) // 移除按钮 通过父节点移除子节点
        }
      }
    })

    Vue.directive('img', {
      inserted(el, binding) {
        if (!binding.value) { // 传过来的地址是undefined或者是null  ''
          // console.log(79997999979)
          el.src = 'https://web87-1316386585.cos.ap-chongqing.myqcloud.com/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20221112153304.jpg'
        }
        el.onerror = () => { // 存在值 但是值加载是过期地址，只要加载失败就监听
          // console.log(1111122222)
          el.src = 'https://web87-1316386585.cos.ap-chongqing.myqcloud.com/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20221112153304.jpg'
        }
      }
    })
  }
}
