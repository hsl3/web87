import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
/* Layout */
import Layout from '@/layout'

// 静态路由表  三个页面不需要额外的权限就可以访问 login  主页  404页面
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: 'Dashboard', icon: 'dashboard' }
    }]
  },
  {
    path: '/import',
    component: Layout,
    children: [{
      path: '/', // ''为空，表示默认要展示的内容
      component: () => import('@/views/employees/import')
    }]
  }
  // 404 page must be placed at the end !!!

]

// 动态路由表 根据用户不同的权限 决定可以访问的页面
// 导入封装好的路由模块
import departments from '@/router/modules/departments'
import employees from '@/router/modules/employees'
import settings from '@/router/modules/settings'
import permission from '@/router/modules/permission'
import salarys from '@/router/modules/salarys'
import social from '@/router/modules/social'
import attendances from '@/router/modules/attendances'
import approvals from '@/router/modules/approvals'
// 动态路由
export const asyncRouter = [
  departments,
  employees,
  settings,
  permission,
  salarys,
  social,
  attendances,
  approvals
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  // 将静态路由和动态路由组合到一起 组成路由表
  // routes: [...constantRoutes, ...asyncRouter]
  routes: [...constantRoutes]
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
