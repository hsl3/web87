import Layout from '@/layout'
export default {
  path: '/attendances',
  component: Layout,
  children: [{
    path: '', // ''为空，表示默认要展示的内容
    name: 'attendances',
    component: () => import('@/views/attendances/attendances'),
    meta: { title: '考勤', icon: 'skill' }
  }]
}
