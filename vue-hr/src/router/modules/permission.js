import Layout from '@/layout'
export default {
  path: '/permission',
  component: Layout,
  children: [{
    path: '', // ''为空，表示默认要展示的内容
    name: 'permissions',
    component: () => import('@/views/permission/permission'),
    meta: { title: '权限管理', icon: 'lock' }
  }]
}
