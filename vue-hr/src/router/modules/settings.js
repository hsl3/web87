import Layout from '@/layout'
export default {
  path: '/settings',
  component: Layout,
  children: [{
    path: '', // ''为空，表示默认要展示的内容
    name: 'settings',
    component: () => import('@/views/settings/settings'),
    meta: { title: '公司设置', icon: 'settings' }
  }]
}
