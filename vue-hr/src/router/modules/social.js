import Layout from '@/layout'
export default {
  path: '/social',
  component: Layout,
  children: [{
    path: '', // ''为空，表示默认要展示的内容
    name: 'social_securitys',
    component: () => import('@/views/social/social'),
    meta: { title: '社保', icon: 'table' }
  }]
}
