import Layout from '@/layout'
export default {
  path: '/employees',
  component: Layout,
  children: [{
    path: '', // ''为空，表示默认要展示的内容
    name: 'employees',
    component: () => import('@/views/employees/employees'),
    meta: { title: '员工管理', icon: 'people' }
  },
  {
    path: 'detail/:id', // 动态传参
    component: () => import('@/views/employees/detail'),
    name: 'employeesDetail',
    hidden: true
  }
  // {
  //   path: 'detail/:id/:name', // 动态传参
  //   component: () => import('@/views/employees/detail'),
  //   name: 'employeesDetail',
  //   hidden: true
  // }
  // {
  //   path: 'detail', // 路由动态传参
  //   name: 'employees',
  //   component: () => import('@/views/employees/detail'),
  //   hidden: true
  // }
  ]
}
