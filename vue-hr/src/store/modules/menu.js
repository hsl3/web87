import { constantRoutes } from '@/router'

export default {
  state() {
    return {
      menuList: []
    }
  },
  mutations: {
    setMenu(state, menu) {
      state.menuList = [...constantRoutes, ...menu]
    }
  },
  actions: {
  },
  getters: {},
  namespaced: true
}
