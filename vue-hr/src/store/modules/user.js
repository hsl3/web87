// import { login, logout, getInfo } from '@/api/user'
// import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'

// const getDefaultState = () => {
//   return {
//     token: getToken(),
//     name: '',
//     avatar: ''
//   }
// }

// const state = getDefaultState()

// const mutations = {
//   RESET_STATE: (state) => {
//     Object.assign(state, getDefaultState())
//   },
//   SET_TOKEN: (state, token) => {
//     state.token = token
//   },
//   SET_NAME: (state, name) => {
//     state.name = name
//   },
//   SET_AVATAR: (state, avatar) => {
//     state.avatar = avatar
//   }
// }

// const actions = {
//   // user login
//   login({ commit }, userInfo) {
//     const { username, password } = userInfo
//     return new Promise((resolve, reject) => {
//       login({ username: username.trim(), password: password }).then(response => {
//         const { data } = response
//         commit('SET_TOKEN', data.token)
//         setToken(data.token)
//         resolve()
//       }).catch(error => {
//         reject(error)
//       })
//     })
//   },

//   // get user info
//   getInfo({ commit, state }) {
//     return new Promise((resolve, reject) => {
//       getInfo(state.token).then(response => {
//         const { data } = response

//         if (!data) {
//           return reject('Verification failed, please Login again.')
//         }

//         const { name, avatar } = data

//         commit('SET_NAME', name)
//         commit('SET_AVATAR', avatar)
//         resolve(data)
//       }).catch(error => {
//         reject(error)
//       })
//     })
//   },

//   // user logout
//   logout({ commit, state }) {
//     return new Promise((resolve, reject) => {
//       logout(state.token).then(() => {
//         removeToken() // must remove  token  first
//         resetRouter()
//         commit('RESET_STATE')
//         resolve()
//       }).catch(error => {
//         reject(error)
//       })
//     })
//   },

//   // remove token
//   resetToken({ commit }) {
//     return new Promise(resolve => {
//       removeToken() // must remove  token  first
//       commit('RESET_STATE')
//       resolve()
//     })
//   }
// }
import { userLogin, userInfo, getUserDetailById } from '@/api/user.js'
import { getToken, setToken as saveToken } from '@/utils/auth'
export default {
  namespaced: true,
  state: {
    // 定义token
    token: getToken() || null, // 先从本地拿  本地没有token 初始值就设置为null
    userInfo: {}
  },
  mutations: { // 第一个参数state : 固定写法,就是vuex的state 第二个参数载荷:是我们调用mutations传递的参数
    setToken(state, newToken) {
      // 1 将token保存到vuex
      state.token = newToken
      // console.log(state)
      //  添加到vuex的token刷新就没有了，所有要做持久化存储token
      saveToken(newToken)
    },
    userInfo(state, payload) {
      // 将用户信息保存到vuex
      state.userInfo = payload
    }
  },
  actions: {
    // 在这里发请求 拿到用户信息保存到vuex
    async userprofile(context, payload) {
      const res = await userInfo(context)
      // console.log(res, 77777777777777)
      // 发请求拿到用户头像 保存到vuex
      const resDetail = await getUserDetailById(res.data.userId)
      // console.log('拿到用户信息和头像信息', res.data, resDetail.data)
      // 将用户名username和头像信息staffPhoto一起通过commit触发mutations 存入userInfo
      context.commit('userInfo', { ...res.data, ...resDetail.data })
      return res.data.roles.menus // 拿到当前登录的这个用户的所有动态权限
    },
    async login(context, payload) {
      // 在这里面发登录请求，如果接口ok  返回token
      // console.log(payload) 表单用户名和密码
      const res = await userLogin(payload)
      // console.log(res)
      context.commit('setToken', res.data)
    },
    logout(context) {
      // 清空token
      context.commit('setToken', '')
      // 删除vuex里面的userInfo
      context.commit('userInfo', {})
      // 只要退出了，就重置路由
      resetRouter()
    }
  }
}

