const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  name: state => {
    return state.user.userInfo.username
  },
  staffPhoto: state => {
    return state.user.userInfo.staffPhoto
  }
}
export default getters
