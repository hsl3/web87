// 如果使用模块系统 (例如通过 vue-cli)，则需要导入 Vue 和 VueI18n ，然后调用 Vue.use(VueI18n)。
import Vue from 'vue'
import VueI18n from 'vue-i18n'

// 引入自定义语言包 英文 中文
import customEN from './en.js'
import customZH from './zh.js'
import locale from 'element-ui/lib/locale'
import elementEN from 'element-ui/lib/locale/lang/en' // 引入饿了么的英文包
import elementZH from 'element-ui/lib/locale/lang/zh-CN' // 引入饿了么的中文包

Vue.use(VueI18n)

// 准备翻译的语言环境信息
const messages = {
  en: {
    // message: {
    //   hello: 'hello world'
    // }
    ...elementEN,
    ...customEN
  },
  zh: {
    // message: {
    //   hello: '你好、世界'
    // }
    ...elementZH,
    ...customZH
  }
}

// 通过选项创建 VueI18n 实例
const i18n = new VueI18n({
  locale: 'lang', // 设置地区
  messages // 设置地区信息
})

// 通过 `i18n` 选项创建 Vue 实例
// new Vue({ i18n }).$mount('#app')

// 配置elementUI 语言转换关系
locale.i18n((key, value) => i18n.t(key, value))

// 导出  然后main.js导入
export default i18n
