import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

// 引入自定义中文包
import customZH from './zh'
// 引入自定义英文包
import customEN from './en'
import locale from 'element-ui/lib/locale'
import elementEN from 'element-ui/lib/locale/lang/en' // 引入饿了么的英文包
import elementZH from 'element-ui/lib/locale/lang/zh-CN' // 引入饿了么的中文包

// 准备翻译的语言环境信息
const messages = {
  en: {
    ...customEN,
    ...elementEN
  },
  zh: {
    ...customZH,
    ...elementZH
  }
}

// 通过选项创建 VueI18n 实例
const i18n = new VueI18n({
  locale: 'zh', // 设置地区
  messages, // 设置地区信息
})
// 配置elementUI 语言转换关系
locale.i18n((key, value) => i18n.t(key, value))
export default i18n