import router, { asyncRoutes } from './router'
console.log(router, 3333);
import store from './store'
console.log(store, 4444);
// import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
// import { getToken } from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'
// import { asyncRoutes } from '@/router'
NProgress.configure({ showSpinner: false }) // NProgress Configuration

// const whiteList = ['/login'] // no redirect whitelist

// router.beforeEach(async(to, from, next) => {
//   // start progress bar
//   NProgress.start()

//   // set page title
//   document.title = getPageTitle(to.meta.title)

//   // determine whether the user has logged in
//   const hasToken = getToken()

//   if (hasToken) {
//     if (to.path === '/login') {
//       // if is logged in, redirect to the home page
//       next({ path: '/' })
//       NProgress.done()
//     } else {
//       const hasGetUserInfo = store.getters.name
//       if (hasGetUserInfo) {
//         next()
//       } else {
//         try {
//           // get user info
//           await store.dispatch('user/getInfo')

//           next()
//         } catch (error) {
//           // remove token and go to login page to re-login
//           await store.dispatch('user/resetToken')
//           Message.error(error || 'Has Error')
//           next(`/login?redirect=${to.path}`)
//           NProgress.done()
//         }
//       }
//     }
//   } else {
//     /* has no token*/

//     if (whiteList.indexOf(to.path) !== -1) {
//       // in the free login whitelist, go directly
//       next()
//     } else {
//       // other pages that do not have permission to access are redirected to the login page.
//       next(`/login?redirect=${to.path}`)
//       NProgress.done()
//     }
//   }
// })

// next() 进入 to 指定的页面
// next(新的页面地址) 重新跳转一次，进入新的页面地址

// 白名单： 未登录用户，也可以访问的页面
const whiteList = ['/login', '/404']

// NProgress是一个第三方的插件，用来在页面切换（路由跳转）显示进度条动画
// NProgress.start() 开启动画
// NProgress.done()  结束动画

router.beforeEach(async (to, from, next) => {
  console.log(`导航守卫: ${from.path} ---->  ${to.path}`)
  NProgress.start() // 开启动画
  // 1. 如果是登录用户
  //    访问/login时，让他跳转到主页(next('/'))
  //    访问不是/login，正常放行next()
  // 2. 如果不是登录用户
  //    访问/login, 正常放行next()
  //    除了/login之外的页面时，跳转到登录next('/login')
  const token = store.state.user.token
  //   to.path: 要访问的页面地址
  if (token) {
    // 发请求拿到用户信息，保存到vuex
    if (to.path === '/login') {
      next('/')
      NProgress.done() // 结束动画
    } else {
      // 有token && 访问的页面不是login && 没有userInfo信息
      // 发请求了
      /* 实现动态添加 */
      // this.$router.addRoutes()
      if (!store.state.user.userInfo.userId) {
        /* 获取用户信息===menus */
        const { menus } = await store.dispatch('user/userProfile')
        console.log(menus, 7788);
        /*根据menus从asyncRoutes中 筛选动态的权限路由规则 */
        /* 筛选出动态路由规则 */
        var newArr = asyncRoutes.filter(route => {
          return menus.includes(route.children[0].name) // 存在就会返回一个true
        })

        newArr.push({ path: '*', redirect: '/404', hidden: true })

        console.log(newArr, 7777);
        /* addRoutes：在这个动态添加路由规则解决了：可以在浏览器上输入路由值访问页面 */
        router.addRoutes(newArr)  // 动态添加8个路由规则，左侧菜单是不会显示的
        /* 将动态的权限存到vuex的menu模块中 */
        // this.$store.commit()
        store.commit('menu/setMenu', newArr)  //  显示左侧菜单的
        /* 解决动态添加的路由规则因为放行的时候有可能没有找到，所以需要重新再来执行一次路由规则 */
        menus.includes(to.path.substring(1)) ? next({ path: to.path, replace: true }) : next('/')
        // next({ path: to.path, replace: true })
        // next(to.path)
      } else {
        next()
      }
      console.log(to.path, 9999888);
    }
  } else {
    // if (to.path === '/login' || to.path === '/404' || to.path === '/c') {
    // if (to.path 在 whiteList 中) {
    if (whiteList.includes(to.path)) {
      next()
    } else {
      console.log('没有登录，进入login')
      next('/login')
      NProgress.done() // 结束动画
    }
  }
})

router.beforeEach((to, from, next) => {
  // console.log(to)
  // document.title = '人资- ' + to.meta.title
  document.title = getPageTitle(to.meta.title)
  next() //
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
