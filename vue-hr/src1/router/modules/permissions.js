import Layout from '@/layout'
export default {
  path: '/permissions',
  component: Layout,
  children: [{
    path: '', // '' 为空，表示默认要展示的内容
    name: 'permissions',
    component: () => import('@/views/permissions/permissions'),
    meta: { title: '权限管理', icon: 'lock' }
  }]
}
