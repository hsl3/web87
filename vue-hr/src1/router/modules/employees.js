import Layout from '@/layout'
export default {
  path: '/employees',
  component: Layout,
  children: [{
    path: '', // '' 为空，表示默认要展示的内容
    name: 'employees',
    component: () => import('@/views/employees/employees'),
    meta: { title: 'employees', icon: 'people' }
  },
  {
    path: 'detail/:id',  // detail/123/jack
    component: () => import('@/views/employees/detail'),
    name: 'employeesDetail',
    hidden: true
  }
    // {
    //   path: 'detail/:id/:name',  // detail/123/jack
    //   component: () => import('@/views/employees/detail'),
    //   name: 'employeesDetail',
    //   hidden: true
    // }
    // {
    //   path: 'detail',
    //   component: () => import('@/views/employees/detail'),
    //   name: 'employeesDetail',
    //   hidden: true
    // }
  ]

}
