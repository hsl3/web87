import Layout from '@/layout'
export default {
  path: '/salarys',
  component: Layout,
  children: [{
    path: '', // '' 为空，表示默认要展示的内容
    name: 'salarys',
    component: () => import('@/views/salarys/salarys'),
    meta: { title: '工资管理', icon: 'money' }
  }]
}
