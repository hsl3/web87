import Layout from '@/layout'
export default {
  path: '/departments',
  // component: () => import('@/views/departments/departments')

  component: Layout,
  children: [{
    component: () => import('@/views/departments/departments'),
    path: '', // '' 为空，表示默认要展示的内容
    name: 'departments',
    meta: { title: 'departments', icon: 'tree' },

  }]
}
