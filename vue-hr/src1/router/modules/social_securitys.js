import Layout from '@/layout'
export default {
  path: '/social_securitys',
  component: Layout,
  children: [{
    path: '', // '' 为空，表示默认要展示的内容
    name: 'social_securitys',
    component: () => import('@/views/social_securitys/social_securitys'),
    meta: { title: '社保', icon: 'table' }
  }]
}
