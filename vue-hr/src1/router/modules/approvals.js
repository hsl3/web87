import Layout from '@/layout'
export default {
  path: '/approvals',
  component: Layout,
  children: [{
    path: '', // '' 为空，表示默认要展示的内容
    name: 'approvals',
    component: () => import('@/views/approvals/approvals'),
    meta: { title: '审批', icon: 'tree-table' }
  }]
}
