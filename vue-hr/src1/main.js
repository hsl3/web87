import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'
import i18n from './lang'

import '@/icons' // icon

// 路由导航守卫 ===> 鉴权
import '@/permission' // permission control

// set ElementUI lang to EN
Vue.use(ElementUI)
// 如果想要中文版 element-ui，按如下方式声明
// Vue.use(ElementUI)

Vue.config.productionTip = false

import obj from '@/components/index.js'
Vue.use(obj)

//全局注册自定义指令 Vue.directive('指令的名称',对象)/* 自定义指令 */
// Vue.directive('allow', {
//   inserted (el, binding) {
//     console.log(el, binding.value, 9999);
//     el.value = binding.value
//     el.focus()
//   }
// })
// Vue.directive('allow', {
//   inserted (el, binding) {
//     // console.log(el, binding.value, store.state.user.userInfo.roles.points, 9999);
//     var point = store.state.user.userInfo.roles.points //[aa,export-excel]
//     var flag = point.includes(binding.value)
//     // console.log(flag, 9999);
//     if (!flag) {
//       // el.style.display = "none"
//       el.parentNode.removeChild(el)
//     }
//   }
// })

new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: h => h(App)
})

console.log('process.env', process.env)
