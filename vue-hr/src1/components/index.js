// 全局注册
// Vue.component(组件名, 组件对象)
import store from '@/store'
import PageTools from '@/components/PageTools'

import MyCom from '@/components/MyCom'
import UploadImg from '@/components/UploadImg'
import ImageHolder from '@/components/ImageHolder'
import Lang from '@/components/Lang'
import ScreenFull from '@/components/ScreenFull'

// Vue.use()的作用：注册插件，增强Vue的功能
// 格式:
//   Vue.use(对象)，
//   对象中有一个install函数，Vue.use(对象)就调用install函数，并传入Vue
// Vue.use({install(Vue){
//
// }})

export default {
  install: function (Vue) {
    Vue.prototype.fn = () => { alert('fn') }

    // console.log('install...', a === Vue)
    Vue.component('PageTools', PageTools)
    Vue.component('MyCom', MyCom)
    Vue.component('UploadImg', UploadImg)
    Vue.component('ImageHolder', ImageHolder)
    Vue.component('Lang', Lang)
    Vue.component('ScreenFull', ScreenFull)

    Vue.directive('allow', {
      inserted (el, binding) {
        // console.log(el, binding.value, store.state.user.userInfo.roles.points, 9999);
        var point = store.state.user.userInfo.roles.points //[aa,export-excel]
        var flag = point.includes(binding.value)
        console.log(flag, 9999);
        if (!flag) {
          // el.style.display = "none"
          el.parentNode.removeChild(el)
        }
      }
    })

    Vue.directive('img', {
      inserted (el, binding) {
        if (!binding.value) {
          // 传过来的地址是undefined或者是null或者是''
          el.src = 'https://vue-hr-1316386510.cos.ap-chengdu.myqcloud.com/bigUserHeader.png'
        }

        /* 存在值 但是值加载是过期地址 只要加载失败就监听*/
        el.onerror = () => {
          console.log(1112222);
          el.src = 'https://vue-hr-1316386510.cos.ap-chengdu.myqcloud.com/bigUserHeader.png'
        }
      }
    })
  }
}

