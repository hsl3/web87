// import { login, logout, getInfo } from '@/api/user'
import { resetRouter } from '@/router'

// const getDefaultState = () => {
//   return {
//     token: getToken(),
//     name: '',
//     avatar: ''
//   }
// }

// const state = getDefaultState()

// const mutations = {
//   RESET_STATE: (state) => {
//     Object.assign(state, getDefaultState())
//   },
//   SET_TOKEN: (state, token) => {
//     state.token = token
//   },
//   SET_NAME: (state, name) => {
//     state.name = name
//   },
//   SET_AVATAR: (state, avatar) => {
//     state.avatar = avatar
//   }
// }

// const actions = {
//   // user login
//   login({ commit }, userInfo) {
//     const { username, password } = userInfo
//     return new Promise((resolve, reject) => {
//       login({ username: username.trim(), password: password }).then(response => {
//         const { data } = response
//         commit('SET_TOKEN', data.token)
//         setToken(data.token)
//         resolve()
//       }).catch(error => {
//         reject(error)
//       })
//     })
//   },

//   // get user info
//   getInfo({ commit, state }) {
//     return new Promise((resolve, reject) => {
//       getInfo(state.token).then(response => {
//         const { data } = response

//         if (!data) {
//           return reject('Verification failed, please Login again.')
//         }

//         const { name, avatar } = data

//         commit('SET_NAME', name)
//         commit('SET_AVATAR', avatar)
//         resolve(data)
//       }).catch(error => {
//         reject(error)
//       })
//     })
//   },

//   // user logout
//   logout({ commit, state }) {
//     return new Promise((resolve, reject) => {
//       logout(state.token).then(() => {
//         removeToken() // must remove  token  first
//         resetRouter()
//         commit('RESET_STATE')
//         resolve()
//       }).catch(error => {
//         reject(error)
//       })
//     })
//   },

//   // remove token
//   resetToken({ commit }) {
//     return new Promise(resolve => {
//       removeToken() // must remove  token  first
//       commit('RESET_STATE')
//       resolve()
//     })
//   }
// }

// export default {
//   namespaced: true,
//   state,
//   mutations,
//   actions
// }
import { getUserDetailById, getUserProfile, login as userLogin } from '@/api/user'
import { getToken, setToken as saveToken } from '@/utils/auth'

export default {
  namespaced: true,
  state: {
    userInfo: {},
    token: getToken() || ''
  },
  mutations: {
    setUserInfo (state, newInfo) {
      state.userInfo = newInfo
    },
    setToken (state, newToken) {
      // 1. 保存到vuex
      state.token = newToken
      console.log(state)
      // 2. 做持久化
      saveToken(newToken)
    }
  },
  actions: {
    logout (context) {
      // 本地token删除
      context.commit('setToken', '')
      // 清空userInfo
      context.commit('setUserInfo', {})

      // 3. 重置路由
      resetRouter()
    },
    async userProfile (context) {
      // 发请求拿到用户信息
      const res = await getUserProfile()
      const resDetail = await getUserDetailById(res.data.userId)
      console.log('发请求拿到用户信息', res, resDetail.data)
      context.commit('setUserInfo', { ...res.data, ...resDetail.data })
      return res.data.roles
    },
    async login (context, payload) {
      // 发请求，如果接口OK,会返回token
      const res = await userLogin(payload)
      // console.log('token: ', res.data)
      // // 调用mutation
      context.commit('setToken', res.data)
    }
  }
}

