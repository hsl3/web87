/**
 * Created by PanJiaChen on 16/11/18.
 */

/**
 * Parse the time to string
 * @param {(Object|string|number)} time
 * @param {string} cFormat
 * @returns {string | null}
 */

import arrayToTree from "array-to-tree"
export function parseTime (time, cFormat) {
  if (arguments.length === 0 || !time) {
    return null
  }
  const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    if ((typeof time === 'string')) {
      if ((/^[0-9]+$/.test(time))) {
        // support "1548221490638"
        time = parseInt(time)
      } else {
        // support safari
        // https://stackoverflow.com/questions/4310953/invalid-date-in-safari
        time = time.replace(new RegExp(/-/gm), '/')
      }
    }

    if ((typeof time === 'number') && (time.toString().length === 10)) {
      time = time * 1000
    }
    date = new Date(time)
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  const time_str = format.replace(/{([ymdhisa])+}/g, (result, key) => {
    const value = formatObj[key]
    // Note: getDay() returns 0 on Sunday
    if (key === 'a') { return ['日', '一', '二', '三', '四', '五', '六'][value] }
    return value.toString().padStart(2, '0')
  })
  return time_str
}

/**
 * @param {number} time
 * @param {string} option
 * @returns {string}
 */
export function formatTime (time, option) {
  if (('' + time).length === 10) {
    time = parseInt(time) * 1000
  } else {
    time = +time
  }
  const d = new Date(time)
  const now = Date.now()

  const diff = (now - d) / 1000

  if (diff < 30) {
    return '刚刚'
  } else if (diff < 3600) {
    // less 1 hour
    return Math.ceil(diff / 60) + '分钟前'
  } else if (diff < 3600 * 24) {
    return Math.ceil(diff / 3600) + '小时前'
  } else if (diff < 3600 * 24 * 2) {
    return '1天前'
  }
  if (option) {
    return parseTime(time, option)
  } else {
    return (
      d.getMonth() +
      1 +
      '月' +
      d.getDate() +
      '日' +
      d.getHours() +
      '时' +
      d.getMinutes() +
      '分'
    )
  }
}

/**
 * @param {string} url
 * @returns {Object}
 */
export function param2Obj (url) {
  const search = decodeURIComponent(url.split('?')[1]).replace(/\+/g, ' ')
  if (!search) {
    return {}
  }
  const obj = {}
  const searchArr = search.split('&')
  searchArr.forEach(v => {
    const index = v.indexOf('=')
    if (index !== -1) {
      const name = v.substring(0, index)
      const val = v.substring(index + 1, v.length)
      obj[name] = val
    }
  })
  return obj
}

// 把平铺的数组数据转换成树形数据

// [
//   {id: "2b", pid: "", name: "人事部"}
//   {id: "2c", pid: "", name: "财务部"}
//   {id: "2d", pid: "2c", name: "财务核算部"}
//   {id: "2e", pid: "2c", name: "税务管理部"}
//   {id: "2f", pid: "2c", name: "薪资管理部"}
// ]
//       |
//       |
//       V
// [
//   {id: "2b", pid: "", name: "人事部",children: []},
//   {id: "2c", pid: "", name: "财务部", children: [
//     {id: "2d", pid: "2c", name: "财务核算部"}
//     {id: "2e", pid: "2c", name: "税务管理部"}
//     {id: "2f", pid: "2c", name: "薪资管理部"}
//   ]}
// ]

export function tranListToTreeData (arr) {
  const treeArr = []

  // 帮助快速确定上级
  const map = {}
  arr.forEach(item => {
    item.children = []
    map[item.id] = item
  })
  console.log(map)
  // 写代码
  arr.forEach(item => {
    // 对arr进行循环，对每一个元素item，如果
    // 1. item有上级元素pItem, 把item添加到pItem.children
    // 2. item没有上级元素（根据item.pid去找，找不到元素）， 添加到treeArr
    const pItem = map[item.pid]
    if (pItem) {
      pItem.children.push(item)
    } else {
      treeArr.push(item)
    }
  })
  return treeArr
}

export function tranListToTreeData2 (arr) {
  return arrayToTree(arr, {
    parentProperty: 'pid',
    customID: 'id'
  })
}

// 把excel文件中的日期格式的内容转回成标准时间
// https://blog.csdn.net/qq_15054679/article/details/107712966
export function formatExcelDate (numb, format = '/') {
  const time = new Date((numb - 25567) * 24 * 3600000 - 5 * 60 * 1000 - 43 * 1000 - 24 * 3600000 - 8 * 3600000)
  time.setYear(time.getFullYear())
  const year = time.getFullYear() + ''
  const month = time.getMonth() + 1 + ''
  const date = time.getDate() + ''
  if (format && format.length === 1) {
    return year + format + month + format + date
  }
  return year + (month < 10 ? '0' + month : month) + (date < 10 ? '0' + date : date)
}
