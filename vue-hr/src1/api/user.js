import request from '@/utils/request'
// 登录
export function login (data) {
  return request({
    url: '/sys/login',
    method: 'POST',
    data: data
  })
}
export function getUserProfile () {
  // 这里请求一定要带token
  return request({
    url: '/sys/profile',
    method: 'POST'
  })
}

/**
 * @description: 获取用户头像
 * @param {*} id 用户id
 * @return {*}
 */
export function getUserDetailById (id) {
  return request({
    url: `/sys/user/${id}`
  })
}
export function getInfo (token) {
}
export function logout () {
}
// import request from '@/utils/request'

// export function login(data) {
//   return request({
//     url: '/vue-admin-template/user/login',
//     method: 'post',
//     data
//   })
// }

// export function getInfo(token) {
//   return request({
//     url: '/vue-admin-template/user/info',
//     method: 'get',
//     params: { token }
//   })
// }

// export function logout() {
//   return request({
//     url: '/vue-admin-template/user/logout',
//     method: 'post'
//   })
// }


/**
 * @description: 保存员工信息
 * @param {*} data
 * @return {*}
 */
export function saveUserDetailById (data) {
  return request({
    url: `/sys/user/${data.id}`,
    method: 'put', // 全量修改
    data
  })
}
