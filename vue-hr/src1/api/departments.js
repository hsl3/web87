import request from '@/utils/request'
// 获取部门信息
export function getDepartments() {
  return request({
    url: '/company/department'
  })
}

export const addDepartment = (data) => request.post('/company/department', data)

// export const delDepartment = (id) => request.delete('/company/department/' + id)
export const delDepartment = (id) => request.delete(`/company/department/${id}`)

export const getDepartmentDetail = (id) => request.get(`/company/department/${id}`)

// 保存修改
export const updateDepartment = (data) => request.put(`/company/department/${data.id}`, data)

