
import request from '@/utils/request'

// export function getEmployees() {
// //   return request({
// //     url: '/sys/user/simple',
// //     method: 'get'
// //   })
//   return request.get('/sys/user/simple')
// }

export const getEmployees = () => request.get('/sys/user/simple')

// 员工列表
export const getEmployeeList = (page, size) => request.get('/sys/user', {
  params: {
    page,
    size
  }
})

/**
 * @description: 删除员工
 * @param {*} id 员工id
 * @return {*}
 */
export function delEmployee (id) {
  return request({
    method: 'delete',
    url: `/sys/user/${id}`
  })
}

export function addEmployee (data) {
  return request({
    method: 'post',
    url: '/sys/user',
    data
  })
}
/**
 * @description: 导入excel
 * @param {*} data
 * @return {*}
 */
export function importEmployee (data) {
  return request({
    url: '/sys/user/batch',
    method: 'post',
    data
  })
}


/**
 * @description: 为用户分配角色
 * @param {*} data { id:当前用户id, roleIds:选中的角色id组成的数组 }
 * @return {*}
 */
export function assignRoles (data) {
  return request({
    url: '/sys/user/assignRoles',
    data,
    method: 'put'
  })
}