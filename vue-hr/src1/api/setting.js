import request from '@/utils/request'

// getAListOfRoles
// page, pagesize
export const getRoles = (page, pagesize) => {
  return request.get('/sys/role', {
    params: {
      page, pagesize
    }
  })
}

export const delRole = id => request.delete(`/sys/role/${id}`)

// data: { name: '', description: '' }
export const addRole = data => request.post(`/sys/role`, data)

// data: { id, name: '', description: '' }
export const updateRole = data => request.put(`/sys/role/${data.id}`, data)


/**
 * @description: 获取角色详情
 * @param {*} id 角色id
 * @return {*}
 */
export function getRoleDetail (id) {
  return request({
    url: `/sys/role/${id}`
  })
}


/**
 * 给角色分配权限
 * @param {*} data {id:角色id, permIds:[] 所有选中的节点的id组成的数组}
 * @returns
 */
export function assignPerm (data) {
  return request({
    url: '/sys/role/assignPrem',
    method: 'put',
    data
  })
}