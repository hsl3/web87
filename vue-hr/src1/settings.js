// es6 的模块化   import export
// nodejs的模块化 require  module.exports

module.exports = {

  title: '人资',

  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  fixedHeader: false,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  sidebarLogo: true
}
