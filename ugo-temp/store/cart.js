export default {
  // 为当前模块开启命名空间
  namespaced: true,

  // 模块的 state 数据
  state: () => ({
    // 购物车的数组，用来存储购物车中每个商品的信息对象
    // 每个商品的信息对象，都包含如下 6 个属性：
    // { goods_id, goods_name, goods_price, goods_count, goods_small_logo, goods_state }
    cart: uni.getStorageSync('cart') || [],
  }),

  // 模块的 mutations 方法
  mutations: {
    addToCart(state, goods) {
      console.log(goods, 99);
      /* 需要判断添加的商品在不在购物车，在数量+1 ，不在就添加这件商品*/
      var obj = state.cart.find(item => item.goods_id == goods.goods_id)
      console.log(obj, 11);
      if (obj) {
        obj.goods_count += 1
      } else {
        state.cart.push(goods)
      }
      console.log(state.cart, 888);
      this.commit('m_cart/setStorage')
    },
    setStorage(state) {
      uni.setStorageSync('cart', state.cart);
    }
  },

  // 模块的 getters 属性
  getters: {
    totalCount(state) {
      return state.cart.reduce((sum, item) => sum + item.goods_count, 0)
    }
  },
}
