// pages/third/third.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    profile: {
      avatarUrl: '/tabs/contact-active.png',
      nickName: '微信用户',
    }
  },
  getUserAvatar(ev) {
    // 获取头像对应的地址
    wx.chooseImage({
      success: (res) => {
        const tempFilePaths = res.tempFilePaths
        console.log(tempFilePaths);

         // 上传临时文件
        wx.uploadFile({
          url: 'http://ajax-api.itheima.net/api/file',
          filePath: tempFilePaths[0],
          name: 'avatar',
          success: (res) => {
            console.log(JSON.parse(res.data));
            this.setData({
              'profile.avatarUrl': JSON.parse(res.data).data.url,
            });
          },
        });
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})