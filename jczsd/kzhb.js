/* ??[空值合并运算符]  ?.[可选链操作符] */

// function handle (day) {
//   if (day == 0) {
//     console.log('0');
//   } else if (day == 1) {
//     console.log('1`');
//   } else if (day == 2) {
//     console.log('2');
//   }

//   // switch (day) {
//   //   case '0':
//   //     return 0;
//   //   case '1':
//   //     return 1;
//   //   case '2':
//   //     return 2;
//   // }

//   /* 这个代码如何做优化 */
// }
// console.log(handle('2'));
// function handle (day) {
//   var d = 0
//   var obj = {
//     'n0': 0,
//     'n1': 1,
//     'n2': 2
//   }

//   d = obj[day] ?? -1
//   console.log(d, 999);
// }
// handle('n0')

/* 定义函数 定义传入形参 */
function handle (n, callback) {
  console.log(n);

  // callback()
  /* 方式1： */
  // if (callback) {
  //   callback()
  // }
  /* 方式2： */
  // callback && callback()
  /* 方式3：可选链操作符 */
  callback?.()
}

/* 函数调用  调用传入实参 */
// handle(111, () => {
//   console.log(23222);
// })
handle(111, () => {
  console.log(222);
})