/* 目的：使用递归：1-10 求和 */
function sum (n) {
  /* 终止条件：n<=1
  需要n递减，并且是递归调用
  */
  if (n <= 1) return 1
  return sum(n - 1) + n

  //  10+9+8
}

console.log(sum(100), 8999);