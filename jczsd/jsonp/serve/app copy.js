const express = require('express')
const cors = require('cors')
const app = express()
app.use(express.static('./dist'))

/* 数据库 */
var mysql = require('mysql');
var connection = mysql.createConnection({
  host: 'localhost',
  user: 'admin',
  password: '123456',
  database: 'heima'//数据库的名称
});

connection.connect();


/* 编写sql语句 */
var sql = 'select  *   from  w87'

/* sql语句
查：select  *   from  表的名字

*/
/* 数据库 */


app.use(cors())//  服务器解决跨域
var arr = [
  { id: 1, name: 'jack' },
  { id: 2, name: 'rose' },
  { id: 3, name: 'tom' },
  { id: 4, name: 'lucy' },
]

app.get('/list', (req, res) => {
  console.log(req.query.fn, 888);
  var fn = req.query.fn
  // res.send(arr)
  // res.json(`${fn}(${arr})`)  //fn(arr)
  // res.send(arr)  //fn(arr)  可以返回数据
  // res.send('fn(' + arr + ')')  //fn(arr)

  // var fn1 = `${fn}(${arr})`
  // res.send(fn1)// fn([])
  // res.json('{ id: 1 }')  //fn(arr)

  res.send(fn + '(' + JSON.stringify(arr) + ')') //fn({})
})

app.get('/list2', (req, res) => {
  console.log(req.query.fn, 888);
  var fn = req.query.fn
  var obj = {
    code: 10000,
    msg: '获取数据成功',
    data: {
      id: 1,
      age: 18,
      gewnder: 1
    }
  }
  res.send(fn + '(' + JSON.stringify(obj) + ')') //fn({})
})

app.get('/fetch', (req, res) => {
  /* 前端向后端发送请求 后端向数据库请求数据  connection.query（sql语句,执行的回调） */
  /* 后端向数据库查询数据 */
  connection.query(sql, function (err, result) {
    console.log(result, 666);
    /* 后端获取数据后将数据返回给前端 */
    res.send(result) //fn({})
  });
  // var arr = [
  //   { id: 1, name: 'jack' },
  //   { id: 2, name: 'rose' }
  // ]

})

app.listen(2000, '192.168.72.52', () => {
  console.log('http://192.168.72.52:2000');
})