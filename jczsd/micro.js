// console.log("start");
// setTimeout(() => {
//   console.log("children2")
//   Promise.resolve().then(() => {
//     console.log("children3")
//   })
// }, 0)

// new Promise(function (resolve, reject) {
//   console.log("children4")
//   setTimeout(function () {
//     console.log("children5")
//     resolve("children6")
//   }, 0)
// }).then(res => { // flag
//   console.log("children7")
//   setTimeout(() => {
//     console.log(res)
//   }, 0)
// })

/*
start
children4
children2
children3
children5
children7
children6
*/


// const promise = new Promise((resolve, reject) => {
//   resolve('success1');
//   reject('error');
//   resolve('success2');
// });
// promise.then((res) => {
//   console.log('then:', res);
// }).catch((err) => {
//   console.log('catch:', err);
// })

/*
then:success1

*/

// Promise.resolve().then(() => {
//   console.log('promise1');
//   const timer2 = setTimeout(() => {
//     console.log('timer2')
//   }, 0)
// });
// const timer1 = setTimeout(() => {
//   console.log('timer1')
//   Promise.resolve().then(() => {
//     console.log('promise2')
//   })
// }, 0)
// console.log('start');

/* 

*/

const promise = new Promise((resolve, reject) => {
  console.log(1);
  setTimeout(() => {
    console.log("timerStart");
    resolve("success");
    console.log("timerEnd");
  }, 0);
  console.log(2);
});
promise.then((res) => {
  console.log(res);
});
console.log(4);

/*
1
2
4
timerStart
timerEnd
success
*/