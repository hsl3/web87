function takeTime (i) {
  return new Promise(resolve => {
    console.timeLog('log', i * 10);
    setTimeout(() => resolve(i), 6000);
  });
}
async function printOut (i) {
  const v = await takeTime(i);
  console.timeLog('log', v);
}
async function go () {
  console.time('log');
  return new Promise(resolve => {
    for (let i = 1; i <= 3; i++) {
      setTimeout(() => { printOut(i); }, 1000 * i);
    }
    setTimeout(() => resolve(), 10000); // 怎么判断全部结束更好？
  })
}
go().then(() => console.timeEnd('log')); // 怎么样的输出，为什么？ await/async可不可以拿走，为什么？"
