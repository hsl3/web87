const express = require('express')
const app = express()
// app.get('/', (req, res) => {
//   res.send('1111')
// })

// app.get('/list', (req, res) => {
//   res.send([
//     { id: 1, name: 'jack' },
//     {
//       id: 2, name: 'rose'
//     }
//   ])
// })

const http = require('http').createServer(app)
const io = require('socket.io')(http, { cors: true }) //创建了一个websocket服务器 并且解决了跨域
// 监听与客户端的连接事件
const data = [
  { item: '11111', a: 70, b: 30 },
  { item: '考勤', a: 60, b: 70 },
  { item: '积极性', a: 50, b: 60 },
  { item: '帮助同事', a: 40, b: 50 },
  { item: '自主学习', a: 60, b: 70 },
  { item: '正确率', a: 70, b: 50 }
]
io.on('connection', socket => {
  console.log('服务端连接成功');

  io.emit('cc', data)
  /* 监听浏览器传过来的事件 */
  socket.on('handleJack', (e) => {
    console.log(2222333333, e);
    /* 将jack发过来的消息传给rose */
    io.emit('messageRose', e)
    /* 注释：说明:你的事件从哪里来可以是直接使用socket传回去、
    但是：如果你传的事件需要另一个人来接收那就需要使用io对象来触发这个事件
    */
  })

  socket.on('handleRose', (e) => {
    console.log(2222333333, e);
    /* 将jack发过来的消息传给rose */
    io.emit('messageJack', e)
    /* 注释：说明:你的事件从哪里来可以是直接使用socket传回去、
    但是：如果你传的事件需要另一个人来接收那就需要使用io对象来触发这个事件
    */
  })
});
/*
http:80
https:443


www.baidu.com
*/
http.listen(2000, '192.168.72.52', () => {
  console.log('http://192.168.72.52:2000');
})