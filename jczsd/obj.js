var obj = { id: 1 }

/* 不能给原有的数据赋值，除非这个属性不存在，是后期添加的属性 */
var { id, name = [] } = obj
console.log(id, name);